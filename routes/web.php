<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// public section


// /sterimar2/app/http/routes.php
/*
controllers:
/app/http/controllers/Main/
	HomeController.php
	ProductsController.php
	NasalHealthController.php
	MiscController.php
	ContactUsController.php
	BlogController.php
	QuizController.php
*/		

Route::get('/', 'Main\HomeController@index');

Route::get('our-products', 'Main\ProductsController@index');
Route::get('our-products/{product}', 'Main\ProductsController@view');
Route::get('where-to-buy', 'Main\ProductsController@wtb');

Route::get('about-sterimar', 'Main\NasalHealthController@index');
Route::get('what-is-nasal-hygiene', 'Main\NasalHealthController@whatis');
Route::get('why-sterimar', 'Main\NasalHealthController@why');
Route::get('is-it-for-children', 'Main\NasalHealthController@children');
Route::get('how-to-use', 'Main\NasalHealthController@useage');
Route::get('faqs', 'Main\NasalHealthController@faqs');
Route::get('download/instructions/{slug}', 'Main\NasalHealthController@download');

Route::get('healthcare-professionals', 'Main\MiscController@hepr');

Route::get('terms-and-conditions', 'Main\MiscController@terms');
Route::get('privacy-policy', 'Main\MiscController@privacy');
Route::get('cookie-notice', 'Main\MiscController@cookies');
Route::get('third-party-information-collection', 'Main\MiscController@thirdparty');

// Route::get('mailtest', 'Main\MiscController@mailtest');

Route::get('test', 'Main\QuizController@index');

// news/blog items
// /app/http/Controllers/Main/BlogController.php
// /app/Models/Blog.php
// sterimar-2k18.blog
// sterimar-2k18.categories
// sterimar-2k18.blog_categories
Route::get('news', 'Main\BlogController@index');
Route::get('news/{slug}', 'Main\BlogController@show');

// contact us
// /app/http/Controllers/Main/ContactUsController.php
// /app/http/Requests/ContactRequest.php
// /app/Models/Contact.php
// sterimar-2k18.contacts
Route::get('contact', 'Main\ContactUsController@index');
Route::post('contact', 'Main\ContactUsController@store');

Route::get('mailtest', 'Main\ContactUsController@mailtest');

//Sitemap
Route::get('sitemap', 'Main\MiscController@sitemap');
/*
Route::get('sitemap', function() {
	
   $sitemap = App::make("sitemap");

   $sitemap->setCache('laravel.sitemap', 60);

   if (!$sitemap->isCached()) {

        $sitemap->add(URL::to('/'), date('Y-m-d\TH:i:s'), '1.0', 'daily');
        $sitemap->add(URL::to('our-products'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('our-products/allergy#stop-and-protect'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('our-products/allergy#nasal-hygiene'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('our-products/allergy#test'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('our-products/cold-and-sinus'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('our-products/cold-and-sinus#quote'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('our-products/cold-and-sinus#congestion-relief'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('our-products/baby-and-kids#quote'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('our-products/baby-and-kids#clears-and-unblocks'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('our-products/baby-and-kids#toddlers-and-kids'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('where-to-buy'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('about-sterimar'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('what-is-nasal-hygiene'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('why-sterimar'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('is-it-for-children'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('how-to-use'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('faqs'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('contact-us'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('news'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        // $sitemap->add(URL::to('test'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');
        $sitemap->add(URL::to('policies'), date('Y-m-d\TH:i:s'), '0.9', 'monthly');

        $posts = DB::table('blog')->orderBy('created_at', 'desc')->get();

		if(count($posts) > 0) {
	        foreach ($posts as $post) {
	           $sitemap->add(URL::to($post->slug), $post->created_at, '0.80', 'monthly');
	        }
		}
   }

   return $sitemap->render('xml');
});
*/

// admin section
/* DEFAULT LARAVEL 5.6.x authentication routes */
// https://stackoverflow.com/questions/29183348/how-to-disable-registration-new-user-in-laravel-5
// /vendor/laravel/framework/src/Illuminate/Routing/Router.php

// Authentication Routes...
Auth::routes();
// -----------------------------------

// login
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');

// logut
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
// -----------------------------------
// disable guest registration
// $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// $this->post('register', 'Auth\RegisterController@register');

// route /register to login controller
Route::get('register', 'Auth\LoginController@showLoginForm')->name('register');

// -----------------------------------

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
/* */

// -----------------------------------

Route::group(['prefix' => 'admin'], function() {

	Route::get('/', function() {
		return redirect('admin/summary');
	});
	Route::get('summary', 'Admin\Summary\SummaryController@index');

	// changeed from model binding
	// Route::get('users', 'Admin\Users\UserController@index');
	Route::get('users', [ 'as' => 'admin.users.index', 'uses' => 'Admin\Users\UserController@index']);
	Route::get('users/create', [ 'as' => 'admin.users.create', 'uses' => 'Admin\Users\UserController@create']);
	Route::post('users/store', [ 'as' => 'admin.users.store', 'uses' => 'Admin\Users\UserController@store']);
	Route::get('users/edit/{id}', [ 'as' => 'admin.users.edit', 'uses' => 'Admin\Users\UserController@edit']);
	Route::put('users/update/{id}', [ 'as' => 'admin.users.update', 'uses' => 'Admin\Users\UserController@update']);
	Route::delete('users/destroy/{id}', [ 'as' => 'admin.users.destroy', 'uses' => 'Admin\Users\UserController@destroy']);

	// blog admin routes
	Route::get('blog', 'Admin\Blog\BlogController@index');
	Route::get('blog/create', 'Admin\Blog\BlogController@create');
	Route::post('blog/store', 'Admin\Blog\BlogController@store');
	Route::get('blog/edit/{id}', 'Admin\Blog\BlogController@edit');
	Route::put('blog/update/{id}', 'Admin\Blog\BlogController@update');
	Route::delete('blog/destroy/{id}', 'Admin\Blog\BlogController@destroy');

	// Route::get('sitemap/refresh', 'Admin\Sitemap\SitemapController@refresh');

/*
	Route::controllers([
		'auth'		=> 'Admin\Auth\AuthController',
		'password'	=> 'Admin\Auth\PasswordController',
	]);


	Route::resource('users', 'Admin\Users\UserController');
*/

});
