A customer has submitted a contact request.
<br/>
<br/>
Name: {{$title}} {{$firstname}} {{$lastname}}<br/>
Phone: {{$phone}}<br/>
Email: {{$email}}<br/>
Product: {{$product}}<br/>
Comments: {{$comments}}<br/>
<br/>
--<br/>
This e-mail was sent from a contact form on Sterimar (http://www.sterimar.co.uk/)
