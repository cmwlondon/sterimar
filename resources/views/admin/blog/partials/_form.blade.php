<div class="row">

	<div class="small-12 medium-12 large-12 columns">
		<div class="row ml0">
		{!! Form::label('category', 'Categories') !!}
		@foreach ($categories as $id => $cat)
			<div class="ff mr3">
				<input type="checkbox" name="cat_{{{$id}}}" value="1" 
				<?php if (isset($post) && array_key_exists($id,$post->current_categories))
					echo ' checked="checked"';
				?>/> {{{$cat}}}
			</div>
		@endforeach
		</div>
	</div>

	<div class="small-12 medium-12 large-6 columns">
		{!! Form::label('thumb','Thumbnail Image') !!}
		@if (isset($post) && $post->thumb != '')
		<div id="imgPreviewThumb">
			<img src="/uploads/blog/thumbs/{{{ $post->thumb }}}" class="preview mb1"/>
			<a href="#" class="button tiny bReplaceThumb">Replace</a>
			<a href="#" class="button tiny bRemoveThumb">Remove</a>
		</div>
		@endif
		<div id="imgUploadThumb" class="<?php echo (isset($post) && $post->thumb != '') ? 'hidden' : ''; ?>">
			{!! Form::file('thumb', null, ['id' => 'thumb']) !!}
			{!! $errors->first('thumb', '<small class="error">:message</small>') !!}
			<p><span class="label radius secondary">(JPG files, max size: 2MB)</span></p>
			{!! Form::hidden('clear_thumb', '0', ['id' => 'clear_thumb']) !!}
		</div>
		
	</div>

	<div class="small-12 medium-12 large-6 columns">
		{!! Form::label('image','Wide Image') !!}
		@if (isset($post) && $post->image != '')
		<div id="imgPreview">
			<img src="/uploads/blog/{{{ $post->image }}}" class="preview mb1"/>
			<a href="#" class="button tiny bReplace">Replace</a>
			<a href="#" class="button tiny bRemove">Remove</a>
		</div>
		@endif
		<div id="imgUpload" class="<?php echo (isset($post) && $post->image != '') ? 'hidden' : ''; ?>">
			{!! Form::file('image', null, ['id' => 'image']) !!}
			{!! $errors->first('image', '<small class="error">:message</small>') !!}
			<p><span class="label radius secondary">(JPG files, 1920 x 775, max size: 2MB)</span></p>
			{!! Form::hidden('clear_image', '0', ['id' => 'clear_image']) !!}
		</div>
		
	</div>

	<div class="small-12 columns">
		{!! Form::label('title', 'Title') !!}
		{!! Form::text('title',(isset($post) ? $post->title : null),['placeholder' => '', 'id' => 'title']) !!}
		{!! $errors->first('title', '<small class="error">:message</small>') !!}
	</div>

	<div class="small-12 columns">
		{!! Form::label('slug', 'Slug') !!}
		{!! Form::text('slug',(isset($post) ? $post->slug : null),['placeholder' => '', 'id' => 'slug']) !!}
		{!! $errors->first('slug', '<small class="error">:message</small>') !!}
	</div>

	<div class="small-12 columns mb1">
		{!! Form::label('synopsis', 'Synopsis') !!}
		{!! Form::textarea('synopsis',(isset($post) ? $post->synopsis : null),['placeholder' => '', 'id' => 'synopsis', 'rows' => '2']) !!}
		{!! $errors->first('synopsis', '<small class="error">:message</small>') !!}
	</div>

	<div class="small-12 columns mb1">
		{!! Form::label('body', 'Body') !!}
		{!! Form::textarea('body',(isset($post) ? $post->body : null),['placeholder' => '', 'id' => 'body']) !!}
		{!! $errors->first('body', '<small class="error">:message</small>') !!}
	</div>

	<div class="small-12 columns">
		<ul class="accordion" data-accordion data-allow-all-closed="true">
		  <li class="accordion-item">
		    <a href="#" class="accordion-title">Search Engine Optimisation</a>
		    <div class="accordion-content" data-tab-content>
		    	<div class="row">
			    	<div class="small-12 columns">
						{!! Form::label('meta_title', 'Page Title') !!}
						{!! Form::text('meta_title',(isset($post) ? $post->meta_title : null),['placeholder' => '']) !!}
						{!! $errors->first('meta_title', '<small class="error">:message</small>') !!}
					</div>

					<div class="small-12 columns">
						{!! Form::label('meta_description', 'Page Description') !!}
						{!! Form::text('meta_description',(isset($post) ? $post->meta_description : null),['placeholder' => '']) !!}
						{!! $errors->first('meta_description', '<small class="error">:message</small>') !!}
					</div>
				</div>
		    </div>
		  </li>
		</ul>
	</div>
	
	<div class="small-6 after-6 columns">
		{!! Form::label('created_at', 'Date') !!}
		{!! Form::date('created_at',(isset($post) ? $post->created_at : $now),['placeholder' => '', 'id' => 'created_at']) !!}
		{!! $errors->first('created_at', '<small class="error">:message</small>') !!}
	</div>

	<div class="small-12 columns mt1">
		<h5>Post Status</h5>
		<div class="panel radius">
		  <div class="row">
		  	<div class="columns small-4">{!! Form::radio('status','draft',(isset($post) ? $post->status : null), ['class' => 'mb0']) !!} Draft</div>
			<div class="columns small-4">{!! Form::radio('status','published',(isset($post) ? $post->status : null), ['class' => 'mb0']) !!} Published</div>
			<div class="columns small-4">{!! Form::radio('status','archived',(isset($post) ? $post->status : null), ['class' => 'mb0']) !!} Archived</div>
		  </div>
		</div>
		{!! $errors->first('status', '<small class="error">:message</small>') !!}
	</div>
</div>