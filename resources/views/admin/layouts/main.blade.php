<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>St&eacute;rimar{{ isset($title) ? ' | '.$title : '' }}</title>

    <link rel="stylesheet" href="/css/admin/foundation.css"/>
    <link rel="stylesheet" href="/css/admin/styles.css"/>
    
    @if (isset($pageViewCSS) && $pageViewCSS != '')
      <link rel="stylesheet" href="/css/{{ $pageViewCSS }}.css"/>
    @endif
  </head>
  <body>
  	<a name="top"></a>
    
    @yield('content')
  	
    <script>
      var site_url  = "{{ url('',[]) }}";
      @if (isset($pageViewJS))
        var pageViewJS = "{{ $pageViewJS }}";
      @endif
    </script>

    {!! Html::script('js/libs/requirejs/require.js', ['data-main' => '/js/admin/main.min.js?'.time()]) !!}

    <div id="hb-templates"></div>
  </body>
</html>