@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li class="current">Users</li>
			</ul>
		</div>

		<div class="medium-12 columns">
			@if (isset($users) && $users->count() != 0)
				<table style="width:100%;">
				  <thead>
				    <tr>
				      <th scope="column">Name</th>
				      <th scope="column">Role</th>
				      <th width="115"scope="column">Actions</th>
				    </tr>
				  </thead>
				  <tbody>

					@foreach ($users as $user)
						<tr>
					      <td class="table-middle">{{{ $user->name }}}</td>
					      <td class="table-middle">{{{ $user->role->title }}}</td>
					      <td class="table-buttons">
					      	<button href="#" data-toggle="drop_{{{ $user->id }}}" aria-controls="drop_{{{ $user->id }}}" aria-expanded="false" class="button small dropdown mb0">Options</button>
							<ul class="dropdown-pane" id="drop_{{{ $user->id }}}" data-dropdown data-auto-focus="true">
								<li>{!! link_to_route('admin.users.edit', 'Edit', [$user->id]) !!}</li>
								<li>{!! Form::model($user, ['route' => ['admin.users.destroy', $user->id], 'method' => 'delete', 'id' => 'form_'.$user->id]) !!}
								  		{!! link_to('#', 'Delete', ['onClick' => 'var r = confirm("Are you sure you want to delete this user?"); if (r == true) {document.getElementById("form_'.$user->id.'").submit();} return false;']) !!}
								  	{!! Form::close() !!}
								</li>
							</ul>
							
						  </td>
					    </tr>
					@endforeach
				  </tbody>
				</table>

			@else
				<p>No users yet</p>
			@endif
		</div>

	</div>
@endsection
