@extends('main.layouts.main')


@section('top')

@endsection


@section('content')
	<div class="carousel-header hp_header">
		<div class="bg" style="background-image: url('/images/hp/header2.jpg');">

			<div class="text">
				<h1>Common nasal conditions: <br>natural prevention and management</h1>
				
			</div>

		</div>
	</div>
	<div class="hp_bar">
		<p>FREE CPD MODULE BUILT IN PARTNERSHIP WITH <img alt="" src="/images/hp/hpa.png"></p>
	</div>

	{{--
	<div class="testimonial blue">
		<a name="information"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'information'])
		<img src="/images/quote-left.svg" class="ql"/>
		<img src="/images/quote-right.svg" class="qr"/>
		@include('main.products.partials._page-quotes', ['quotCount' => '1', 'quotRef' => [3]])
	</div>
	--}}


	<div class="row section what-is">
		<div class="columns span-12">

			<div class="lo">
				<img class="clock" alt="30 - 60 mins" src="/images/hp/clock.png">
				<h3><span>LEARNING</span> OBJECTIVES</h3>
				<ul>
					<li>Understand the basic structure of the nasal passages and&nbsp;sinuses</li>
					<li>Be familiar with common nasal conditions and their&nbsp;symptoms</li>
					<li>Gain confidence in offering natural drug-free prevention and management&nbsp;advice</li>
				</ul>
				<h3>CLICK BELOW TO ACCESS THE&nbsp;MODULES</h3>

				<div class="moduleSelect">
					<div class="msBox">
						<div class="spacer mba">
							<div class="frame"><a href="https://www.healthprofessionalacademy.co.uk/mum-and-baby/learn/common-nasal-conditions" target="_blank" title="MUM &amp; BABY ACADEMY"><img alt="MUM &amp; BABY ACADEMY" src="/images/hp/mba.png"></a></div>
							<a href="https://www.healthprofessionalacademy.co.uk/mum-and-baby/learn/common-nasal-conditions" target="_blank" class="buttonx">GP'S AND MIDWIVES</a>
						</div>
					</div>
					<div class="msBox">
						<div class="spacer pa">
							<div class="frame"><a href="https://www.healthprofessionalacademy.co.uk/pharmacy/learn/common-nasal-conditions" target="_blank" title="PHARMACY ACADEMY"><img alt="PHARMACY ACADEMY" src="/images/hp/pa.png"></a></div>
							<a href="https://www.healthprofessionalacademy.co.uk/pharmacy/learn/common-nasal-conditions" target="_blank" class="buttonx">PHARMACISTS</a>
						</div>
					</div>
				</div>
			</div>

			{{--
			<ul class="info">
				<li class="active">
					@include('main.products.partials._expand-collapse')
					<h5>The nose is your first line of defense.</h5>
					<p>The nose is a highly effective filter for your body. Filtering through over 15,000 litres of air a day and wonderful smells such as flowers or a fresh coffee - whilst doing its best to filter out the bad, all that dust, pollution and allergens, looking to enter into your body through your nasal passages. Once trapped, the tiny hairs along the lining of the nasal passages called cilia work to carry away the impurities to the back of the throat where they are swallowed and destroyed before they have a chance to multiply and cause harm.</p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>Help your nose defend against airborne allergens and germs. </h5>
					<p>If the nasal passage (the inside of your nose) dries out or is clogged, it cannot be as efficient when performing its crucial role in keeping you healthy. That is where nasal hygiene comes in. Ensuring that the nose is clean, clear and moisturised can help ensure the nose remains in top working condition to help defend against the airborne contaminants you breathe in. It helps to prevent the onset of symptoms and reduce the risk of further problems occurring.</p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>So nasal hygiene isn&rsquo;t a vanity - it actually helps you stay well and keep breathing?</h5>
					<p>Yeah that&rsquo;s right. Keeping your nose clean improves defences against cold symptoms, rhinopharyngitis and hayfever. To protect the body and do its job properly the nose needs to stay in top condition, that means keeping it clean and clear.</p>
				</li>
			</ul>
			--}}
		</div>

		{{--
			<div class="columns span-3 sm-lg-11 sm-lg-before-1 learn-more">
			<img src="/images/allergy-uk.svg" class="allergyuk"/>
			@include('main.products.partials._side-links')
		</div>
		--}}

	</div>


	{{-- @include('main.products.partials._not-sure') --}}

@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
