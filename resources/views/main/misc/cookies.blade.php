@extends('main.layouts.main')


@section('top')

@endsection


@section('content')
	<div class="row plainText">
		<div class="columns span-8 before-2 md-12 md-before-0 sm-12 sm-before-0">
			<h1>Cookie Notice</h1>
			<p>We have outlined below the types of cookie that are used on this Site.</p>
			{{--
			<h2>Cookies</h2>
			<p>Cookies are small text files that are placed on your computer by a website in order to make websites work, or work more efficiently, as well as to provide information to the owners of the site. Cookies help us to run the Site and improve its interactivity and our services.</p>
			<p>We have outlined below the types of cookie that are used on this Site.</p>
			<h2>Strictly necessary cookies</h2>
			<p>We use first-party cookies to help the Site work in the best possible manner. You may opt out of receiving these cookies and delete them using your internet browser settings. However, your user experience may be impacted.</p>
			<table cellpadding="0" cellspacing="0" border="1" id="cookie-table" class="mt1">
				<tbody>
					<tr>
						<th width="120">Name</th>
						<th>Purpose</th>
						<th width="120">Retention</th>
					</tr>
					<tr>
						<td>XSRF-TOKEN</td>
						<td>Used to ensure data transmitted from forms back to the server is not intercepted by third parties and manipulated on route.</td>
						<td>2 hours</td>
					</tr>
					<tr>
						<td>laravel_session</td>
						<td>Allows the website to communicate with itself as users navigate between pages, holding temporary information such as form error messages.</td>
						<td>2 hours</td>
					</tr>
				</tbody>
			</table>
			<h2>Analytics cookies</h2>
			<p>We use Google Analytics cookies to analyse how this Site is used. Google analytics cookies monitor how visitors navigate the Site and how they found their way here. We use these so that we can see the total amount of visitors to the Site; this does not offer individual information. It also allows us to see what content our users enjoy most which helps us to provide you with the best service. You may at any time change your cookie settings to accept or delete these cookies by <a href="https://tools.google.com/dlpage/gaoptout" target="_blank">clicking here</a>.</p>
			<p>We use a Facebook Widget within the homepage of the website, this utilises a range of cookies to know if you&rsquo;re logged into Facebook in the background, allowing you to LIKE and share posts.</p>
			--}}
			
			<!-- OneTrust Cookies List start -->
			<div id="ot-sdk-cookie-policy">Cookie Notice</div>
			<!-- OneTrust Cookies List end -->
		</div>
	</div>

@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
