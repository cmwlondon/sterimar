{{-- @extends('main.layouts.channel_sight_main') --}}
@extends('main.layouts.main')

@section('top')

@endsection


@section('content')
	<div class="carousel-header">
		<div class="bg" style="background-image: url('/images/products/header-allergy-1.jpg');">

			<div class="text">
				<h1>Allergy<br/><span>relief</span></h1>
				<p>Our allergy and hayfever range of solutions - from prevention to fast and effective relief from symptoms.</p>
			</div>

			{{-- <img src="/images/gp-recommended-brand.png" class="roundal"/> --}}

			<div class="bar">
				<div class="row full">
					<div class="columns span-6 splitL">
						<span>Scroll down to<br>find out more</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="testimonial green">
		<a name="quote"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'quote'])
		<img src="/images/quote-left.svg" class="ql"/>
		<img src="/images/quote-right.svg" class="qr"/>
		@include('main.products.partials._page-quotes', ['quotCount' => '1', 'quotRef' => [0]])
	</div>

	<div class="row full section view green">
		<a name="stop-and-protect" class="cf"></a>

		<div class="bar"></div>
		<div class="buy-wrap">
			<a href="{{{$buyLinks['spar']}}}" target="_blank" class="buy">Buy&nbsp;Now</a>

			{{-- ChannelSight widget reconfigure for data-asset-id and data-product-sku --}}
			{{-- <button class="cswidget" data-asset-id="91" data-product-sku="DemoSKU-US-1">Buy Now </button> --}}

			

		</div>
		<div class="columns col span-12 first-head">
			<h1>Rapid and effective relief from allergies</h1>
		</div>

		<div class="columns span-3 col1 eq hidden-sm-lg" group="g1">
			<div class="wrap down lift eq-inner">
				<img src="/images/products/stop-and-protect.png" class="laze" />
				{{-- <img src="/images/products/stop-and-protect.png" class="laze cswidget" data-asset-id="91" data-product-sku="DemoSKU-US-1" /> --}}
			</div>
		</div>

		<div class="columns span-6 sm-lg-12 col2 eq" group="g1">
			<div class="eq-inner">

				<h2 class="uc ml0">Stop <span>&amp; Protect</span></h2>
				<h3 class="ml0">Allergy Response</h3>
				<ul class="info">
					<li class="active">
						@include('main.products.partials._expand-collapse')
						<h5>Puts a stop to symptoms fast.</h5>
						<p>ST&Eacute;RIMAR<sup>TM</sup> Stop &amp; Protect Allergy Response nasal pump is scientifically proven to provide rapid and effective relief from the symptoms of allergic rhinitis including blocked, runny, itchy nose, sneezing and tingly and watery eyes. With a base of natural sea water and a patented complex, this innovative formula, enriched with sea minerals, unblocks nasal passages and encapsulates, inactivates and neutralises the allergens. Putting a stop to symptoms fast.</p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>Reinforced effective protection against further attacks.</h5>
						<p> Our solution, inspired by nature, provides long-lasting hydration and forms a protective and invisible film, preventing allergens from coming into contact with the nasal cavity to provide reinforced effective protection against further allergic reactions and chronic conditions developing.</p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>Fast and effective relief when needed most.</h5>
						<p>Suitable for pregnant women, breastfeeding mothers and children from 3 years. The solution, inspired by nature, is this non-drowsy and preservative-free product - the latest edition to the trusted ST&Eacute;RIMAR<sup>TM</sup> hayfever range of 100% natural sea water nasal sprays.</p>
					</li>
				</ul>
			</div>
		</div>

		<div class="columns span-3 sm-lg-7 col4 eq hidden vis-sm-lg">
			<div class="wrap down eq-inner">
				<img src="/images/products/stop-and-protect.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-3 sm-lg-5 col3 eq" group="g1">
			<div class="wrap eq-inner">
				{{-- <img src="/images/allergy-uk.svg" class="allergyuk"/> --}}
				@include('main.products.partials._side-links')
			</div>
		</div>
	</div>



	<div class="row full section view green">
		<a name="hayfever-relief"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'hayfever-relief'])

		<div class="bar"></div>
		<div class="buy-wrap"><a href="{{{$buyLinks['nhas']}}}" target="_blank" class="buy">Buy&nbsp;Now</a></div>
		<div class="columns col span-12">
			<h1>Prevents allergy symptoms</h1>
		</div>

		<div class="columns span-3 col1 eq hidden-sm-lg" group="g2">
			<div class="wrap down lift eq-inner">
				<img src="/images/products/nasal-hygeine.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-6 sm-lg-12 col2 eq" group="g2">
			<div class="eq-inner">

				<h2 class="uc ml0">Hayfever &amp; Allergy relief</h2>
				<ul class="info">
					<li class="active">
						@include('main.products.partials._expand-collapse')
						<h5>Prevents allergy symptoms</h5>
						<p>ST&Eacute;RIMAR<sup>TM</sup> Hayfever &amp; Allergy Relief is an isotonic solution that helps clear and cleanse nasal passages, eliminating allergens in contact with nasal lining (pollen, dust, dust mites, pet&nbsp;hair&hellip;).</p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>Helps reduce the number of acute allergic rhinitis episodes by&nbsp;42%***.</h5>
						<p>Helps prevent allergic reactions from reoccurring by washing out nasal&nbsp;cavities.<br><br><span class="disclaimer">***Study on 60 adults where 30 used St&eacute;rimar daily for 5 months alongside standard treatment (M. Grasso et al., 2018)</span></p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>Free from drugs, steroids and preservatives – From the UK’s No.1 recommended non-medicated nasal spray brand*.</h5>
						<p>ST&Eacute;RIMAR<sup>TM</sup> natural isotonic** formula is free from drugs, steroids and preservatives, making it suitable for daily use from 3 years and up. ST&Eacute;RIMAR<sup>TM</sup> Hayfever &amp; Allergy Relief can also be used by pregnant/breastfeeding women and may be used following nose&nbsp;surgery.
						<br><br><span class="disclaimer">*GPrX data: non-medicated nasal sprays, UK, from July 2019 to June 2020.</span>
						<br><br><span class="disclaimer">**Isotonic solution: solution with the same salt concentration as cells in the human&nbsp;body.</span></p>
					</li>
				</ul>
			</div>
		</div>

		<div class="columns span-3 sm-lg-7 col4 eq hidden vis-sm-lg">
			<div class="wrap down eq-inner">
				<img src="/images/products/nasal-hygeine.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-3 sm-lg-5 col3 eq" group="g2">
			<div class="wrap eq-inner">
				<!-- allergyUK image /public/images/allergy-uk.svg -->
				@include('main.products.partials._side-links')

			</div>
		</div>
	</div>



	<div class="row full section alt green interested">
		<div class="columns col span-12">
			<h1>You may also be interested in</h1>
		</div>
		<div class="row threecol">
			<div class="columns">
				<a href="/our-products/nasal-hygiene-breathe-easy-daily"><img src="/images/products/alt-nasal-hygiene.png" class="packs"/></a>
				<h3>Nasal Hygiene</h3>
			</div>
			<div class="columns">
				<a href="/our-products/cold-and-sinus"><img src="/images/products/alt-cold-and-sinus.png" class="packs"/></a>
				<h3>Cold and Sinus</h3>
			</div>
			<div class="columns">
				<a href="/our-products/baby-and-kids"><img src="/images/products/alt-baby-and-kids.png" class="packs"/></a>
				<h3>Baby and Kids</h3>
			</div>
		</div>
	</div>

	@include('main.products.partials._not-sure')

@endsection

@section('footer')
	@include('main.layouts.partials._footer', ['footnote' => '*GPRX data: Sodium Chloride sub-category: data from April 2016 to April 2018.'])
@endsection
