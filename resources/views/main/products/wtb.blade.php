@extends('main.layouts.main')

@section('top')

@endsection

@section('content')
	<div class="carousel-header">
		<div class="bg align-top" style="background-image: url('/images/products/header-home.jpg');">
			<div class="overlay"></div>
			<div class="middle wtb">
				<h1>Not sure which St&eacute;rimar <br/><span>is right for you?</span></h1>
			    <p class="link"><a href="/test" class="quiz">Take The Test</a></p>
			</div>
		</div>
	</div>

	<div class="section wtb">
		<a name="retailers"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'retailers'])

		<div id="wtb_wrap" class="row animatedParent animateOnce" data-sequence="100">

			<transition
				appear
				mode="out-in"
				@before-enter="beforeEnter"
		    @enter="enter"
				@before-leave="beforeLeave"
		    @leave="leave"
		    v-bind:css="false">

				<div v-cloak id="english_wtb_select" v-if="displayWTB === 'start'" key="start">
					<div class="columns span-12">
						<div class="row">
							<div class="columns span-12">
								<h2>SELECT YOUR COUNTRY:</h2>
							</div>
							<div class="columns sm-up-12 md-up-6 lg-up-4 lg-up-before-1 lg-up-after-1 english-flag" @click="showEnglish">
								<div class="inner">
									<img src="/images/products/english_flag.png" class="border" alt="">
									<div class="fade-background"></div>
									<h4>UK</h4>
								</div>
							</div>
							<div class="columns sm-up-12 md-up-6 lg-up-4 lg-up-before-1 lg-up-after-1 irish-flag" @click="showIrish">
								<div class="inner">
									<img src="/images/products/irish_flag.png" alt="">
									<div class="fade-background"></div>
									<h4>IRELAND</h4>
								</div>
							</div>
						</div>
					</div>
				</div>

				{{-- ENGLAND RETAILERS --}}
				<div v-cloak id="english_wtb" v-if="displayWTB === 'english'" key="english">
					<div class="columns span-12">
						<div class="row">
							<div class="columns sm-up-6 sm-up-before-3 sm-up-after-3 md-up-2 md-up-after-0 md-up-before-0 xlg-up-1 english-flag">
								<img class="flag-border" src="/images/products/english_flag.png" @click="showStart" alt="">
							</div>
							<div class="columns sm-up-12 md-up-10">
								<p>You can buy Sterimar online or in-store at Waitrose &amp; Partners, Sainsbury’s, Morrisons, Tesco Pharmacy, Lloyds Pharmacy, Weldricks Pharmacy, Rowlands Pharmacy, Independent Pharmacies, Chemist4u, +Well Pharmacy, Amazon, Holland &amp; Barrett, Superdrug and Boots</p>
							</div>
						</div>
					</div>

					<div class="row retailers">

						<div class="columns sm-up-12 md-up-12 lg-up-4">
							<a href="https://www.waitrose.com/ecom/shop/search?&searchTerm=sterimar" target="_blank" class="retailer" data-label="Waitrose" title="Buy Now">
								<img src="/images/retailers/waitrose.png" class="logo pixel" data-id="3" alt="Waitrose" />
							</a>
						</div>
						<div class="columns sm-up-12 md-up-12 lg-up-4">
							<a href="https://www.sainsburys.co.uk/webapp/wcs/stores/servlet/SearchDisplayView?catalogId=10241&storeId=10151&langId=44&krypto=mIIHlOYLnU5DszfUXf1SYeGHFaHqIHdG4wicZ80gJe8bYKb4%2FC%2BTqv717Yu%2BSXgCaYgt9ReQEg5xummoH8%2FtnLHZdjwCSWDGCegPB7ulYelmJZQOXAkYPHPM%2B8K5MiJj%2FGCD7YQo5VqgQZr2mViMpWJi%2B9jlaV5sJEmCaxmcCLw%3D#langId=44&storeId=10151&catalogId=10241&categoryId=&parent_category_rn=&top_category=&pageSize=60&orderBy=&searchTerm=sterimar&beginIndex=0&hideFilters=true&categoryFacetId1=" target="_blank" class="retailer" data-label="Sainsburys" title="Buy Now">
								<img src="/images/retailers/sainsburys.png" class="logo pixel" data-id="3" alt="Sainsburys" />
							</a>
						</div>
						<div class="columns sm-up-12 md-up-12 lg-up-4">
							<a href="https://groceries.morrisons.com/webshop/getSearchProducts.do?clearTabs=yes&isFreshSearch=true&chosenSuggestionPosition=&entry=Sterimar#KeT1WhiDcHs5ZBT1.97" target="_blank" class="retailer" data-label="morrisons" title="Buy Now">
								<img src="/images/retailers/morrisons.png" class="logo" data-id="5" alt="Morrisons" />
							</a>
						</div>

						<div class="columns sm-up-12 md-up-12 lg-up-4">
							<!-- <a href="#[tesco-pharmacy]" target="_blank" class="retailer" data-label="tesco-pharmacy" title="Buy Now"> -->
								<img src="/images/retailers/tesco_pharmacy.png" class="logo" data-id="2" alt="Tesco Pharmacy" />
							<!-- </a> -->
						</div>

						<div class="columns sm-up-12 md-up-12 lg-up-4">
							<a href="https://lloydspharmacy.com/products/sterimar-isotonic-nasal-hygiene-100ml" target="_blank" class="retailer" data-label="lloyds-pharmacy" title="Buy Now">
								<img src="/images/retailers/llyods-pharmacy.png" class="logo" data-id="6" alt="Lloyds Pharmacy" />
							</a>
						</div>

						<div class="columns sm-up-12 md-up-12 lg-up-4">
							<a href="https://www.weldricks.co.uk/brands/sterimar" target="_blank" class="retailer" data-label="Weldricks Pharmacy" title="Buy Now"><img src="/images/retailers/weldricks-pharmacy.png" class="logo pixel" data-id="11" alt="Weldricks Pharmacy" /></a>
						</div>

						<div class="columns sm-up-12 md-up-12 lg-up-4">
								<img src="/images/retailers/rowlands.png" class="logo" data-id="7" alt="Rowlands Pharmacy" />
						</div>

						<div class="columns sm-up-12 md-up-12 lg-up-4">
								<img src="/images/retailers/independent-pharmacies.png" class="logo" data-id="8" alt="Independent Pharmacies" />
						</div>

						<div class="columns sm-up-12 md-up-12 lg-up-4"><a href="https://www.chemist-4-u.com/catalogsearch/result/?dir=desc&q=Sterimar" target="_blank" class="retailer" data-label="tesco-pharmacy" title="Buy Now"><img src="/images/retailers/chemist4u.png" class="logo pixel" data-id="9" alt="Chemist 4u" /></a></div>

						<div class="columns sm-up-12 md-up-12 lg-up-4">
							<a href="https://shop.well.co.uk/product/3356276" target="_blank" class="retailer" data-label="pluswell" title="Buy Now">
								<img src="/images/retailers/pluswell.png" class="logo pixel" data-id="10" alt="pluswell" />
							</a>
						</div>

						<div class="columns sm-up-12 md-up-12 lg-up-4">
							<a href="https://www.amazon.co.uk/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=sterimar" target="_blank" class="retailer" data-label="amazon" title="Buy Now">
								<img src="/images/retailers/amazon.png" class="logo pixel" data-id="10" alt="Amazon" />
							</a>
						</div>
						<div class="columns sm-up-12 md-up-12 lg-up-4">
							<a href="https://www.hollandandbarrett.com/search?query=sterimar&isSearch=true" target="_blank" class="retailer" data-label="holland-and-barrett" title="Buy Now">
								<img src="/images/retailers/holland-barrett2.jpg" class="logo pixel" data-id="4" alt="Holland &amp; Barrett" />
							</a>
						</div>
						<div class="columns sm-up-12 md-up-12 lg-up-4">
							<a href="https://www.superdrug.com/b/Sterimar" target="_blank" class="retailer" data-label="superdrug" title="Buy Now">
								<img src="/images/retailers/superdrug.png" class="logo pixel" data-id="3" alt="Superdrug" />
							</a>
						</div>
						<div class="columns sm-up-12 md-up-12 lg-up-4">
							<a href="http://www.boots.com/webapp/wcs/stores/servlet/SolrSearchLister?storeId=10052&langId=-1&catalogId=11051&stReq=1&searchTerm=sterimar#container" target="_blank" class="retailer" data-label="boots" title="Buy Now">
								<img src="/images/retailers/boots.png" class="logo" data-id="11" alt="Boots" />
							</a>
						</div>
						<!--  -->
					</div>
				</div>

				{{-- IRISH RETAILERS --}}
				<div v-cloak id="irish_wtb" v-if="displayWTB === 'irish'" key="irish">

					<div class="columns span-12">
						<div class="row">
							<div class="columns sm-up-6 sm-up-before-3 sm-up-after-3 md-up-2 md-up-after-0 md-up-before-0 xlg-up-1 irish-flag">
								<img class="flag-border" src="/images/products/irish_flag.png" @click="showStart" alt="">
							</div>
							<div class="columns sm-up-12 md-up-10">
								<p>You can buy St&eacute;rimar from a range of independent pharmacies:</p>
							</div>
						</div>
					</div>

					<div class="row retailers independent">


						<div class="columns sm-up-12 md-up-12 lg-up-4"><img src="/images/retailers/wtb/your-local-pharmacy-logo-website.jpg" class="logo"/></div>
						<div class="columns sm-up-12 md-up-12 lg-up-4"><a href="https://www.chtralee.com/p/sterimar-breath-easy-daily/3331300097221" target="_blank" title="CH Tralee"><img alt="CH Tralee" src="/images/retailers/wtb/ch-tralee.png" class="logo"/></a></div>
						<div class="columns sm-up-12 md-up-12 lg-up-4"><img src="/images/retailers/wtb/health-express-logo-ref-website.jpg" class="logo"/></div>

						<div class="columns sm-up-12 md-up-12 lg-up-4 mccabe"><a href="https://www.mccabespharmacy.com/catalogsearch/result/?q=Sterimar" target="_blank" title=""><img src="/images/retailers/wtb/mccabes-logo-ref-website-mccabespharmacy.com.png" class="logo"/></a></div>
						<div class="columns sm-up-12 md-up-12 lg-up-4"><img src="/images/retailers/wtb/mcsharrylogo-ref-website.jpg" class="logo"/></div>
						<div class="columns sm-up-12 md-up-12 lg-up-4 meagher"><a href="https://www.meagherspharmacy.ie/search?q=Sterimar" target="_blank" title="Buy Now"><img src="/images/retailers/wtb/meaghers-logo-stacked-website-meaghers.ie.jpg" class="logo"/></a></div>

						<div class="columns sm-up-12 md-up-12 lg-up-4"><img src="/images/retailers/wtb/mulligans-logo.png" class="logo"/></div>
						<div class="columns sm-up-12 md-up-12 lg-up-4"><img src="/images/retailers/wtb/mullingar-logo-ref-website.jpeg" class="logo"/></div>
						<div class="columns sm-up-12 md-up-12 lg-up-4"><img src="/images/retailers/wtb/phelans-family-pharmacy-logo-ref-website-phelans.ie.png" class="logo"/></div>

						<div class="columns sm-up-12 md-up-12 lg-up-4 sammccauley"><img src="/images/retailers/wtb/mccauley.png" class="logo"/></div>
						<div class="columns sm-up-12 md-up-12 lg-up-4"><img src="/images/retailers/wtb/stacks-pharmacy-logo-ref-website-stackspharmacy.ie.png" class="logo"/></div>
						<div class="columns sm-up-12 md-up-12 lg-up-4"><img src="/images/retailers/wtb/totalhealth-logo-ref-website-totalhealth.ie.jpg" class="logo"/></div>
					</div>

					<div class="columns sm-up-12">
						<p class="pTB-2">Lloyds Pharmacy, Tesco Pharmacy, Amazon and Boots</p>
					</div>

					<div class="row retailers independent">
						<div class="columns sm-up-12 md-up-12 lg-up-4">
							<a href="https://www.lloydspharmacy.ie/pregnancy-und-child-health/baby-infant-und-child-medicines/sterimar-baby-nasal-spray-50ml" target="_blank" class="retailer" data-label="llyods-pharmacy" title="Buy Now"><img src="/images/retailers/llyods-pharmacy.png" class="logo" data-id="4"/></a>
						</div>
						<div class="columns sm-up-12 md-up-12 lg-up-4">
							<!-- <a href="#" target="_blank" class="retailer" data-label="boots" title="Buy Now"> -->
							<img src="/images/retailers/tesco_pharmacy.png" class="logo" data-id="2"/>
							<!-- </a> -->
						</div>
						<div class="columns sm-up-12 md-up-12 lg-up-4">
							<a href="http://www.boots.ie/brands/sterimar" target="_blank" class="retailer" data-label="boots" title="Buy Now">
								<img src="/images/retailers/boots.png" class="logo" data-id="1"/>
							</a>
						</div>
						<div class="columns sm-up-12 md-up-12 lg-up-4 offset-md-3">
							<a href="https://www.amazon.co.uk/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=sterimar" target="_blank" class="retailer" data-label="amazon" title="Buy Now">
								<img src="/images/retailers/amazon.png" class="logo" data-id="10" alt="Amazon" />
							</a>
						</div>
					</div>
				</div>

			</transition>
		</div>
	</div>

@endsection

@section('footer')
	@include('main.layouts.partials._footer', ['footnote' => '*GPRX data: Sodium Chloride sub-category: data from April 2016 to April 2018.'])
@endsection
