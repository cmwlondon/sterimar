@extends('main.layouts.main')

@section('top')

@endsection

@section('content')
	{{-- blade comment --}}
	
	<div class="carousel-header">
		<div class="bg" style="background-image: url('/images/products/header-nasal-hygiene-1.jpg');">
			<div class="text">
				<h1>NASAL HYGIENE<br/><span>Breathe Easy&nbsp;Daily</span></h1>
				<p>Cleanses and eliminates&nbsp;impurities. <br>Helps to breathe better.</p>
			</div>

			<img src="/images/products/mvp-award-2020.png" class="original roundal"/>

			<div class="bar">
				<div class="row full">
					<div class="columns span-6 splitL">
						<span>Scroll down to<br>find out more</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="testimonial blue">
		<a name="quote"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'quote'])
		<img src="/images/quote-left.svg" class="ql"/>
		<img src="/images/quote-right.svg" class="qr"/>
		@include('main.products.partials._page-quotes', ['quotCount' => '1', 'quotRef' => [3]])
	</div>

	<div class="row full section view blue">
		

		<div class="bar"></div>
		<div class="buy-wrap"><a href="{{{$buyLinks['nasalhygiene']}}}" target="_blank" class="buy">Buy&nbsp;Now</a></div>
		

		<a name="breathe-easy-daily" class="cf"></a>
		<div class="columns col span-12 first-head">
			<h1>NATURALLY CLEARS AND <br>CLEANSES THE&nbsp;NOSE</h1>
		</div>

		<div class="columns span-3 col1 eq hidden-sm-lg" group="g1">
			<div class="wrap down lift eq-inner">
				<img src="/images/products/nasal-hygiene-01.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-6 sm-lg-12 col2 eq" group="g1">
			<div class="eq-inner">

				<h2 class="uc ml0">BREATHE EASY DAILY</h2>
				<h3 class="ml0">Nasal Hygiene</h3>
				<ul class="info">
					<li class="active">
						@include('main.products.partials._expand-collapse')
						<h5>Isotonic formula. Cleanses and eliminates impurities. Helps to breathe&nbsp;better.</h5>
						<p>ST&Eacute;RIMAR<sup>TM</sup> original isotonic formula Breathe Easy Daily is a 100% Natural Sea Water based nasal spray which can be used daily to help cleanse the nose and eliminate impurities from the nasal passages. Daily use can help improve breathing and so enhancing the quality of sleep.</p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>Helps prevent ear, nose and throat disorders.</h5>
						<p>ST&Eacute;RIMAR<sup>TM</sup> Breathe Easy Daily helps prevent ENT disorders by strengthening the nasal lining’s defences.</p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>Free from drugs, steroids and preservatives – From the UK’s No.1 recommended non-medicated nasal spray brand*</h5>
						<p>ST&Eacute;RIMAR<sup>TM</sup> natural isotonic** formula is free from drugs, steroids and preservatives, making it suitable for daily use from 3 years and up. STÉRIMAR™ Breathe Easy Daily can also be used by pregnant/breastfeeding women and may be used following nose surgery.
						<br><br><span class="disclaimer">*GPrX data: non-medicated nasal sprays, UK, from July 2019 to June 2020.</span>
						<br><br><span class="disclaimer">**Isotonic solution: solution with the same salt concentration as cells in the human body.</span></p>
					</li>
				</ul>
			</div>
		</div>

		<div class="columns span-3 sm-lg-7 col4 eq hidden vis-sm-lg">
			<div class="wrap down eq-inner">
				<img src="/images/products/nasal-hygiene-01.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-3 sm-lg-5 col3 eq" group="g1">
			<div class="wrap eq-inner">
				<!-- <img src="/images/allergy-uk.svg" class="allergyuk"/> -->
				@include('main.products.partials._side-links')
			</div>
		</div>
	</div>

	<div class="row full section alt blue interested">
		<div class="columns col span-12">
			<h1>You may also be interested in</h1>
		</div>
		<div class="row threecol">
			<div class="columns">
				<a href="/our-products/allergy"><img src="/images/products/alt-allergy.png" class="packs"/></a>
				<h3>Allergy Relief</h3>
			</div>
			<div class="columns">
				<a href="/our-products/cold-and-sinus"><img src="/images/products/alt-cold-and-sinus.png" class="packs"/></a>
				<h3>Cold and Sinus</h3>
			</div>
			<div class="columns">
				<a href="/our-products/baby-and-kids"><img src="/images/products/alt-baby-and-kids.png" class="packs"/></a>
				<h3>Baby and Kids</h3>
			</div>
		</div>
	</div>

	@include('main.products.partials._not-sure')

@endsection

@section('footer')
	@include('main.layouts.partials._footer', ['footnote' => '*GPRX data: Sodium Chloride sub-category: data from April 2016 to April 2018.'])
@endsection
