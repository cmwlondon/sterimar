@extends('main.layouts.main')


@section('top')

@endsection


@section('content')
	<div class="carousel-header">
		<div class="bg" style="background-image: url('/images/products/header-cold-and-sinus.jpg');">

			<div class="text">
				<h1>Congestion<br/><span>relief</span></h1>
				<p>From Cold Defence to Cold &amp; Sinus Relief, beat winter congestion with our range of naturally inspired solutions.</p>
			</div>

			{{-- <img src="/images/gp-recommended-brand.png" class="roundal"/> --}}

			<div class="bar">
				<div class="row full">
					<div class="columns span-6 splitL">
						<span>Scroll down to<br>find out more</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="testimonial orange">
		<a name="quote"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'quote'])
		<img src="/images/quote-left.svg" class="ql"/>
		<img src="/images/quote-right.svg" class="qr"/>
		@include('main.products.partials._page-quotes', ['quotCount' => '1', 'quotRef' => [3]])
	</div>

	<div class="row full section view orange">
		<a name="cold-defence"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'cold-defence'])

		<div class="bar"></div>
		<div class="buy-wrap"><a href="{{{$buyLinks['scd']}}}" target="_blank" class="buy">Buy&nbsp;Now</a></div>
		<div class="columns col span-12 first-head">
			<h1>Helps limit cold symptoms</h1>
		</div>

		<div class="columns span-3 col1 eq hidden-sm-lg" group="g1">
			<div class="wrap down eq-inner">
				<img src="/images/products/cold-defence.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-6 sm-lg-12 col2 eq" group="g1">
			<div class="eq-inner">

				<h2 class="uc ml0">Cold <span>Defence</span></h2>
				<h3 class="ml0">Colds &amp; ENT disorders</h3>
				<ul class="info">
					<li class="active">
						@include('main.products.partials._expand-collapse')
						<h5>Naturally gentle and effective</h5>
						<p>Get an extra layer of protection against colds with new ST&Eacute;RIMAR<sup>TM</sup> Cold Defence – an exclusive formula inspired by nature. The 100% sea water based solution is enriched with copper salts to gently cleanse nasal passages and help reduce the risk of ENT disorders such as sinusitis, bronchitis and otitis.</p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>Washes away impurities</h5>
						<p>ST&Eacute;RIMAR<sup>TM</sup> Cold Defence washes out impurities in the nose including airbornes responsible for colds, helping to limit cold symptoms from developing and enabling you to make the most of your busy day ahead.</p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>From the UK&rsquo;s No.1 recommended non-medicated nasal spray brand*</h5>
						<p>Like all ST&Eacute;RIMAR<sup>TM</sup> products, Cold Defence is free from steroids, drugs and preservatives, suitable for pregnant women and the whole family from 3 years and up.
						<br><br><span class="disclaimer">*GPrX data: non-medicated nasal sprays, UK, from July 2019 to June 2020.</span></p>
					</li>
				</ul>
			</div>
		</div>

		<div class="columns span-3 sm-lg-7 col4 eq hidden vis-sm-lg">
			<div class="wrap down eq-inner">
				<img src="/images/products/cold-defence.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-3 sm-lg-5 col3 eq" group="g1">
			<div class="wrap eq-inner">
				@include('main.products.partials._side-links')
			</div>
		</div>
	</div>

	<div class="row full section view orange">
		<a name="stop-and-protect"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'stop-and-protect'])

		<div class="bar"></div>
		<div class="buy-wrap"><a href="{{{$buyLinks['spcs']}}}" target="_blank" class="buy">Buy&nbsp;Now</a></div>
		<div class="columns col span-12 first-head">
			<h1>Immediate relief from congestion</h1>
		</div>

		<div class="columns span-3 col1 eq hidden-sm-lg" group="g1">
			<div class="wrap down eq-inner">
				<img src="/images/products/cold-and-sinus-relief.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-6 sm-lg-12 col2 eq" group="g1">
			<div class="eq-inner">

				<h2 class="uc ml0">Stop <span>&amp; Protect</span></h2>
				<h3 class="ml0">Cold &amp; Sinusitis Relief</h3>
				<ul class="info">
					<li class="active">
						@include('main.products.partials._expand-collapse')
						<h5>Fast and effective relief from congestion - acts in 2 minutes</h5>
						<p>ST&Eacute;RIMAR<sup>TM</sup> Stop &amp; Protect<sup>TM</sup> Cold &amp; Sinusitis Relief is scientifically proven to <strong>act in 2 minutes</strong>, to unblock and relieve nasal passages. The hypertonic solution with a base of 100% natural sea water creates a natural osmotic effect to gently draw and drain even thick mucus, allowing you to breathe more naturally and helping prevent secondary&nbsp;infection*.<br><br><span class="disclaimer">*by washing out nasal&nbsp;cavities.</span></p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>Protects from further infection</h5>
						<p>Our patented formula, inspired by nature provides long lasting hydration and forms a protective invisible film on the nasal lining. This helps to strengthen the nose&rsquo;s barrier function and rapidly helps neutralise germs responsible for colds, helping protect you from future&nbsp;infection.</p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>Scientifically proven and fast acting relief - From the UK’s No.1 recommended non-medicated nasal spray brand*.</h5>
						<p>Made with 100% natural purified sea water based formula, enriched in sea minerals with added copper and eucalyptus oil. ST&Eacute;RIMAR<sup>TM</sup> Stop &amp; Protect<sup>TM</sup> Cold and Sinusitis Relief is drug, steroid and preservative free. It is suitable for adults and children from 3 years and&nbsp;up.<br><br>
						<span class="disclaimer">*GPrX data: non-medicated nasal sprays, UK, from July 2019 to June 2020.</span></p>
<!--p>Made with 100% natural purified sea water based formula, enriched in sea minerals with added copper and eucalyptus oil. ST&Eacute;RIMAR<sup>TM</sup> Stop &amp; Protect<sup>TM</sup> Cold and Sinusitis Relief is drug, steroid and preservative free. It is suitable for pregnant women, breast feeding mothers and children from 3 years and&nbsp;up.</p-->

					</li>
				</ul>
			</div>
		</div>

		<div class="columns span-3 sm-lg-7 col4 eq hidden vis-sm-lg">
			<div class="wrap down eq-inner">
				<img src="/images/products/cold-and-sinus-relief.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-3 sm-lg-5 col3 eq" group="g1">
			<div class="wrap eq-inner">
				@include('main.products.partials._side-links')
			</div>
		</div>
	</div>




	<div class="row full section view orange">
		<a name="congestion-relief"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'congestion-relief'])

		<div class="bar"></div>
		<div class="buy-wrap"><a href="{{{$buyLinks['crcr']}}}" target="_blank" class="buy">Buy&nbsp;Now</a></div>
		<div class="columns col span-12">
			<h1>Congestion relief for all the family</h1>
		</div>

		<div class="columns span-3 col1 eq hidden-sm-lg" group="g2">
			<div class="wrap down eq-inner">
				<img src="/images/products/congestion-relief.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-6 sm-lg-12 col2 eq" group="g2">
			<div class="eq-inner">

				<h2 class="uc ml0">Congestion <span>relief</span></h2>
				<h3 class="ml0">Colds &amp; Rhinitis</h3>
				<ul class="info">
					<li class="active">
						@include('main.products.partials._expand-collapse')
						<h5>Powerful yet gentle decongestant, inspired by nature.</h5>
						<p>ST&Eacute;RIMAR<sup>TM</sup> Congestion Relief is a hypertonic* solution made from 100% natural sea water, enriched with added copper and magnesium salts to help prevent flu contamination**, wash away even thick mucus and rapidly decongest the nose in case of colds and sinusitis.
						<br>ST&Eacute;RIMAR<sup>TM</sup> Congestion Relief helps to fight colds, sinusitis and limit the risks of secondary infection by washing out nasal cavities with no known side effects from chemicals like steroids or preservatives  that some medications may have.
						<br><br><span class="disclaimer">* Hypertonic solution: solution with a higher salt concentration than cells in the human body for a natural decongestant effect, called the osmotic effect.</span>
						<br><br><span class="disclaimer">** If exposed to flu airbornes</span></p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>Helps to prevent further infections developing.</h5>
						<p>By cleaning and clearing the nasal passages, ST&Eacute;RIMAR<sup>TM</sup> Congestion Relief helps to rebalance nasal primary functions – removing congestants that block the nose and not allowing it to work properly.</p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>Free from drugs, preservatives and steroids - From the UK’s No.1 recommended non-medicated nasal spray brand*.</h5>
						<p>This makes it suitable for all the family including babies from 3 years, pregnant and breastfeeding women – and means it can be used as long as required, 2 to 6 times a day or as directed by your doctor.
						<br><br>
					<span class="disclaimer">*GPrX data: non-medicated nasal sprays, UK, from July 2019 to June 2020.</span></p>
					</li>
				</ul>
			</div>
		</div>

		<div class="columns span-3 sm-lg-7 col4 eq hidden vis-sm-lg">
			<div class="wrap down eq-inner">
				<img src="/images/products/congestion-relief.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-3 sm-lg-5 col3 eq" group="g2">
			<div class="wrap eq-inner">
				@include('main.products.partials._side-links')
			</div>
		</div>
	</div>

	<div class="row full section alt orange interested">
		<div class="columns col span-12">
			<h1>You may also be interested in</h1>
		</div>
		<div class="row threecol">
			<div class="columns">
				<a href="/our-products/nasal-hygiene-breathe-easy-daily"><img src="/images/products/alt-nasal-hygiene.png" class="packs"/></a>
				<h3>Nasal Hygiene</h3>
			</div>
			<div class="columns">
				<a href="/our-products/allergy"><img src="/images/products/alt-allergy.png" class="packs"/></a>
				<h3>Allergy Relief</h3>
			</div>
			<div class="columns">
				<a href="/our-products/baby-and-kids"><img src="/images/products/alt-baby-and-kids.png" class="packs"/></a>
				<h3>Baby and Kids</h3>
			</div>
		</div>
	</div>

	@include('main.products.partials._not-sure')

@endsection

@section('footer')
	@include('main.layouts.partials._footer', ['footnote' => '*GPRX data: Sodium Chloride sub-category: data from April 2016 to April 2018.'])
@endsection
