<div class="carousel-footer no-hide">
	<a name="test"></a>
	@include('main.layouts.partials._arrow', ['hrefAnchor' => 'test'])
	<div class="bg" style="background-image: url('/images/products/header-home.jpg');">
		<div class="overlay"></div>
		<div class="middle">
			<h1>Not sure which St&eacute;rimar <br/><span>is right for you?</span></h1>
		    <p class="link"><a href="/test#take-the-test" class="quiz">Take The Test</a></p>
		</div>
	</div>
</div>