<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 44 44" style="enable-background:new 0 0 44 44;" xml:space="preserve">
	<path class="plus" d="M22,44C9.9,44,0,34.1,0,22S9.9,0,22,0c12.1,0,22,9.9,22,22S34.1,44,22,44z M22,2C11,2,2,11,2,22
		c0,11,9,20,20,20c11,0,20-9,20-20C42,11,33,2,22,2z M24.7,34.1h-5.4v-9.4H9.9v-5.4h9.4V9.9h5.4v9.4h9.4v5.4h-9.4V34.1z"/>
	<path class="minus" d="M22,44C9.9,44,0,34.1,0,22C0,9.9,9.9,0,22,0c12.1,0,22,9.9,22,22C44,34.1,34.1,44,22,44z M22,2C11,2,2,11,2,22
		c0,11,9,20,20,20c11,0,20-9,20-20C42,11,33,2,22,2z M9.9,19.3h24.1v5.4H9.9V19.3z"/>
</svg>