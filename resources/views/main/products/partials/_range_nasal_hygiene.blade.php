<div class="row full section blue nasal_hygiene1">
	<a name="breathe-easy"></a>
	@include('main.layouts.partials._arrow', ['hrefAnchor' => 'breathe-easy'])

	<div class="bar"></div>

	<div class="columns span-6 md-12 sm-12 col1 eq lg-up-pull-right" group="blocked">

		<div class="eq-inner">
			<h1>NASAL HYGIENE<br/><span>Breathe Easy Daily</span></h1>
			<h2>Isotonic formula. <br>Cleanses and eliminates impurities. <br>Helps to breathe better.</h2>
			<div class="bottom">
				<a href="/our-products/nasal-hygiene-breathe-easy-daily" class="">NASAL HYGIENE Products</a>
			</div>
		</div>
	</div>
	<div class="columns span-6 md-12 sm-12 col2 eq" group="blocked">
		<div class="wrap eq-inner">
			<img src="/images/products/nasal-hygiene-01.png" class="nasal-hygiene" />
		</div>
	</div>


</div>
