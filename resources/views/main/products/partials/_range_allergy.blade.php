<div class="row full section green">
	<a name="allergy-sufferer"></a>
	@include('main.layouts.partials._arrow', ['hrefAnchor' => 'allergy-sufferer'])

	<div class="bar"></div>

	<div class="columns span-6 md-12 sm-12 col2 eq lg-up-pull-right" group="allergy">
		<div class="eq-inner">
			<h1>Allergy<br/><span>sufferer?</span></h1>
			<h2>Our allergy and hay fever range provides rapid and effective relief from symptoms and reinforced protection against further attacks.</h2>
			<div class="bottom">
				<a href="/our-products/allergy" class="">Allergy Products</a>
			</div>
		</div>
	</div>

	<div class="columns span-6 md-12 sm-12 col1 eq" group="allergy">
		<div class="wrap eq-inner">
			<img src="/images/products/range-allergy.png"/>
		</div>
	</div>


</div>
