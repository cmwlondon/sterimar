@for($i = 0; $i < $quotCount; $i++)

    {{-- ALLERGY PRODUCTS --}}
    @if(isset($quotRef[$i]) && $quotRef[$i] == 0)
      <blockquote class="fade">
  		  <p>St&eacute;rimar helps me to stop sneezing, stop feeling terrible, stop having red streaming eyes and a headache. It means I can go out wherever I like, whenever I like.</p>
  		  <footer>Cherry Edwards, July 2016</footer>
  		</blockquote>
    @elseif(isset($quotRef[$i]) && $quotRef[$i] == 1)
      <blockquote class="fade">
        <p>St&eacute;rimar helps me enjoy the summer!! Stops my sneeze, stops my wheeze and makes going in the great outdoors a total breeze!</p>
        <footer>Louise Blackwell, July 2016</footer>
      </blockquote>
    @elseif(isset($quotRef[$i]) && $quotRef[$i] == 2)
      <blockquote class="fade">
        <p>St&eacute;rimar helps me get through the summer months with our two young children. We absolutely love the outdoors amd this helps us to enjoy it more without worrying about hayfever!</p>
        <footer>Nicola Phipps, July 2016</footer>
      </blockquote>
    @elseif(isset($quotRef[$i]) && $quotRef[$i] == 3)
      <blockquote class="fade">
        <p>St&eacute;rimar helps me to sleep more comfortably at night because I can breathe more clearly. I love that it's natural and not full of nasties. I suffer bad with my sinuses and this stuff works a treat!</p>
        <footer>Zoe Jones, July 2016</footer>
      </blockquote>

    {{-- CONGESTION RELIEF PRODUCTS --}}
    @elseif(isset($quotRef[$i]) && $quotRef[$i] == 3)
      <blockquote class="fade">
        <p>St&eacute;rimar helps me to sleep more comfortably at night because I can breathe more clearly. I love that it's natural and not full of nasties. I suffer bad with my sinuses and this stuff works a treat!</p>
        <footer>Zoe Jones, July 2016</footer>
      </blockquote>
    @elseif(isset($quotRef[$i]) && $quotRef[$i] == 4)
      <blockquote class="fade">
        <p>St&eacute;rimar is the only thing I have used that actually helps with nasal congestion. I would recommend it to anyone suffering from a cold.</p>
        <footer>Amy Thomas, February 2016</footer>
      </blockquote>
    @elseif(isset($quotRef[$i]) && $quotRef[$i] == 5)
      <blockquote class="fade">
        <p>Stérimar is the only thing I have used that actually helps with nasal congestion. I would recommend it to anyone suffering from a cold.</p>
        <footer>Amy Thomas, February 2016</footer>
      </blockquote>

    {{-- BABY & KIDS PRODUCTS --}}
    @elseif(isset($quotRef[$i]) && $quotRef[$i] == 6)
      <blockquote class="fade">
        <p>I love St&eacute;rimar as its drug free so can use it all year round without nasty side effects or recongestion. Would recommend it for the whole family....even young children!</p>
        <footer>Heather Hancock, February 2016</footer>
      </blockquote>
    @elseif(isset($quotRef[$i]) && $quotRef[$i] == 7)
      <blockquote class="fade">
        <p>St&eacute;rimar helps me to get through the summer months with our two young children. We absolutely love the outdoors and this helps us to enjoy it more without worrying about hayfever!</p>
        <footer>Nicola Phipps, July 2016</footer>
      </blockquote>
    @elseif(isset($quotRef[$i]) && $quotRef[$i] == 8)
      <blockquote class="fade">
        <p>St&eacute;rimar helps me to get a better night&rsquo;s sleep when I use the baby version on my children when they are all stuffy. I highly recommend so parents can get some much needed rest when they are unwell with blocked/runny noses.</p>
        <footer>Kayleigh Parker, July 2016</footer>
      </blockquote>

    @endif
@endfor
