<div class="row full section blue">
	<a name="is-it-for-chuildren"></a>
	@include('main.layouts.partials._arrow', ['hrefAnchor' => 'is-it-for-chuildren'])

	<div class="bar"></div>

	@if ($isWinter)

	<div class="columns span-6 md-12 sm-12 col1 eq" group="kids">
		<div class="eq-inner">
			<h1>Is it for<br/><span>children?</span></h1>
			<h2>Our range of specially designed solutions – to clear and unblock little noses and keep them in top working condition.</h2>
			<div class="bottom">
				<a href="/our-products/baby-and-kids" class="">Kids and Baby Products</a>
			</div>
		</div>
	</div>

	<div class="columns span-6 md-12 sm-12 col2 eq" group="kids">
		<div class="wrap eq-inner">
			{{-- @include('main.layouts.partials._mumsnet', ['page' => 'our-products2']) --}}
			<img src="/images/products/range-kids-and-baby-plus.png"/>
		</div>
	</div>

	@else

	<div class="columns span-6 md-12 sm-12 col2 eq lg-up-pull-right" group="kids">
		<div class="eq-inner">
			<h1>Is it for<br/><span>children?</span></h1>
			<h2>Our range of specially designed solutions – to clear and unblock little noses and keep them in top working condition.</h2>
			<div class="bottom">
				<a href="/our-products/baby-and-kids" class="">Kids and Baby Products</a>
			</div>
		</div>
	</div>

	<div class="columns span-6 md-12 sm-12 md-pull-right col1 eq" group="kids">
		<div class="wrap eq-inner">
			{{-- @include('main.layouts.partials._mumsnet', ['page' => 'our-products2']) --}}
			<img src="/images/products/range-kids-and-baby-plus.png"/>
		</div>
	</div>

	@endif

</div>
