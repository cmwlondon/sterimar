<div class="row full section orange">
	<a name="blocked-nose"></a>
	@include('main.layouts.partials._arrow', ['hrefAnchor' => 'blocked-nose'])

	<div class="bar"></div>

	<div class="columns span-6 md-12 sm-12 col1 eq" group="blocked">

		<div class="eq-inner">
			<h1>Blocked<br/><span>nose?</span></h1>
			<h2>Help limit cold symptoms and rapidly decongest the nose with the STÉRIMAR<sup>TM</sup> Cold and Congestion range.</h2>
			<div class="bottom">
				<a href="/our-products/cold-and-sinus" class="">Cold and Sinus Products</a>
			</div>
		</div>
	</div>

	<div class="columns span-6 md-12 sm-12 col2 eq" group="blocked">
		<div class="wrap eq-inner">
			<img src="/images/products/range-cold-and-sinus.png"/>
		</div>
	</div>

</div>
