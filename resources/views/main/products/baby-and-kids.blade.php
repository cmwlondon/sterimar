@extends('main.layouts.main')


@section('top')

@endsection


@section('content')
	<div class="carousel-header">
		<div class="bg" style="background-image: url('/images/products/header-baby-and-kids.jpg');">

			<div class="text">
				<h1>Clears and unblocks<br/><span>little noses</span></h1>
				<p>Our range of specially designed solutions – to clear and unblock little noses and keep them in top working condition.</p>
			</div>

			{{-- <img src="/images/gp-recommended-brand.png" class="roundal"/> --}}

			<div class="bar">
				<div class="row full">
					<div class="columns span-6 splitL">
						<span>Scroll down to<br>find out more</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="testimonial blue">
		<a name="quote"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'quote'])
		<img src="/images/quote-left.svg" class="ql"/>
		<img src="/images/quote-right.svg" class="qr"/>
		@include('main.products.partials._page-quotes', ['quotCount' => '1', 'quotRef' => [6]])
	</div>

	<div class="row full section view blue">
		<a name="clears-and-unblocks"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'clears-and-unblocks'])
		<div class="bar"></div>
		<div class="buy-wrap"><a href="{{{$buyLinks['bcu']}}}" target="_blank" class="buy">Buy&nbsp;Now</a></div>
		<div class="columns col span-12">
			<h1>Clears and unblocks nasal passages from&nbsp;birth</h1>
		</div>

		<div class="columns span-3 col1 eq hidden-sm-lg" group="g1">
			<div class="wrap down eq-inner">
				
				<img src="/images/products/baby.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-6 sm-lg-12 col2 eq" group="g1">
			{{-- @include('main.layouts.partials._mumsnet', ['page' => 'baby-and-kids2']) --}}
			<div class="eq-inner">

				<h2 class="uc ml0">BREATHE EASY BABY</h2>
				<!-- <h3 class="ml0">Clears &amp; Unblocks</h3> -->
				<ul class="info">
					<li class="active">
						@include('main.products.partials._expand-collapse')
						<h5>Clears and unblocks nasal passages from birth.</h5>
						<p>In the first few months of our lives we mainly breathe through our noses. This means that a stuffy nose can be very distressing for both you and your little one, causing problems with breathing, sleeping and eating.
							<br>ST&Eacute;RIMAR<sup>TM</sup>  is a gentle yet effective solution to help you keep your baby&rsquo;s nose clean and clear and help your little one breathe naturally.
							<br>Breathe Easy Baby is trusted by millions of mums all over the world.
						<span class="disclaimer" style="display:none;">disclaimer</span></p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>Helps prevent colds and aid natural breathing.</h5>
						<p>By clearing impurities and germ filled mucus from your baby&rsquo;s nose, Breathe Easy Baby not only helps ensure your little one can breath naturally but also helps support the nose&rsquo;s natural filtering function. This helps ensure it is working as effectively as possible as a first line of defense, helping to prevent infections and allergic reactions.</p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>Why choose Breathe Easy Baby?</h5>
						<p>Breathe Easy Baby is a 100% natural purified sea water based nasal spray that gently cleans and clears little noses. The ST&Eacute;RIMAR<sup>TM</sup> patented nozzle has a gentle micro-diffusion spray to coat the inside of the nasal passages and ensure easy breathing and the prevention of colds. Breathe Easy Baby nozzle now has an ergonomic shape and non-slip, comfortable coating surface to ensure an easy and stress free application every time. Breathe Easy Baby contains no preservatives or additives making it the ideal solution to use from birth to 3 years.</p>
					</li>
				</ul>
			</div>
		</div>

		<div class="columns span-3 sm-lg-7 col4 eq hidden vis-sm-lg">
			<div class="wrap down eq-inner">
				{{-- @include('main.layouts.partials._mumsnet', ['page' => 'baby-and-kids']) --}}
				<img src="/images/products/baby.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-3 sm-lg-5 col3 eq" group="g1">
			<div class="wrap eq-inner">
				<!-- allergyUK image /public/images/allergy-uk.svg -->
				@include('main.products.partials._side-links')

			</div>
		</div>
	</div>

	<div class="row full section view blue">
		<a name="toddlers-and-kids"></a>
		{{-- @include('main.layouts.partials._arrow', ['hrefAnchor' => 'toddlers-and-kids']) --}}

		<div class="bar"></div>
		<div class="buy-wrap"><a href="{{{$buyLinks['kcrcr']}}}" target="_blank" class="buy">Buy&nbsp;Now</a></div>
		<div class="columns col span-12 first-head">
			<h1>Unblocks congested little noses from 3 months</h1>
		</div>

		<div class="columns span-3 col1 eq hidden-sm-lg" group="g1">
			<div class="wrap down eq-inner">
				<img src="/images/products/toddlers-and-kids.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-6 sm-lg-12 col2 eq" group="g1">
			<div class="eq-inner">

				<h2 class="uc ml0">CONGESTION RELIEF <span>KIDS</span></h2>
				<ul class="info">
					<li class="active">
						@include('main.products.partials._expand-collapse')
						<h5>The effective natural solution that unblocks congested little noses from 3 months and up.</h5>
						<p>ST&Eacute;RIMAR<sup>TM</sup> Kids Congestion Relief is a hypertonic* solution made from 100% natural sea water enriched with added copper and magnesium salts.  ST&Eacute;RIMAR<sup>TM</sup> developed a new patented safety nozzle with non-slipping, comfortable coated surface to ensure gentle but effective relief from little blocked noses. With a slightly higher salt concentration than cells in the human body, ST&Eacute;RIMAR<sup>TM</sup> Kids creates a natural osmotic effect that gently draws and drains excess fluid, like mucus, from the nasal lining to rapidly relieve bunged up noses and help prevent the risk of secondary infection by washing out nasal cavities.
						<br><br><span class="disclaimer">* Hypertonic solution: solution with a higher salt concentration than cells in the human body for a natural decongestant effect, called the osmotic effect.</span></p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>Helps fight colds.</h5>
						<p>Children&rsquo;s immune systems haven&rsquo;t had as long to develop defences against the germs that cause colds. ST&Eacute;RIMAR<sup>TM</sup> Kids helps rebalance the nose&rsquo;s natural defences, giving them a helping hand against those germs that spread around the classroom and cause runny noses - to limit the risk of further infections. </p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>Why choose ST&Eacute;RIMAR<sup>TM</sup> Kids.</h5>
						<p>ST&Eacute;RIMAR<sup>TM</sup> Kids is a 100% natural purified sea water based solution that is completely free from drugs and preservatives. The solution, rich in sea minerals and copper salts, works in harmony with your child&rsquo;s body to gently relieve congestion and help support their bodies natural defenses. The new nozzle is safe to use and can be used from any angle to disperse the solution in very fine droplets for increased efficacy. </p>
					</li>
				</ul>
			</div>
		</div>

		<div class="columns span-3 sm-lg-7 col4 eq hidden vis-sm-lg">
			<div class="wrap down eq-inner">
				<img src="/images/products/toddlers-and-kids.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-3 sm-lg-5 col3 eq" group="g1">
			<div class="wrap eq-inner">
				<!-- allergyUK image /public/images/allergy-uk.svg -->
				@include('main.products.partials._side-links')

			</div>
		</div>
	</div>



	<div class="row full section view blue">
		<a name="baby-stop-and-protect"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'clears-and-unblocks'])
		<div class="bar"></div>
		<div class="buy-wrap"><a href="{{{$buyLinks['bsap']}}}" target="_blank" class="buy">Buy&nbsp;Now</a></div>
		<div class="columns col span-12">
			<h1>IMMEDIATE RELIEF FROM CONGESTION FROM 3&nbsp;MONTHS</h1>
		</div>

		<div class="columns span-3 col1 eq hidden-sm-lg" group="g1">
			<div class="wrap down eq-inner">
				
				<img src="/images/products/baby-stop-and-protect.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-6 sm-lg-12 col2 eq" group="g1">
			<div class="eq-inner">

				<h2 class="uc ml0">BABY STOP &amp; PROTECT&trade; COLD</h2>
				<!-- <h3 class="ml0">Clears &amp; Unblocks</h3> -->
				<ul class="info">
					<li class="active">
						@include('main.products.partials._expand-collapse')
						<h5>Fast and effective relief from congestion</h5>
						<p>ST&Eacute;RIMAR&trade; Baby Stop &amp; Protect&trade; Cold is a scientifically proven formula that decongests and relieves baby’s nasal passages, helping them to breathe easier. Our solution fluidifies and facilitates the discharge of mucus, helping fight the cold by washing the nasal cavity. With a base of natural sea water and a patented complex, this innovative formula, high in trace elements and enriched with copper salts, fights cold symptoms quickly and&nbsp;effectively.
						<span class="disclaimer" style="display:none;">disclaimer</span></p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>100% natural sea water based solution</h5>
						<p>ST&Eacute;RIMAR&trade; Baby Stop &amp; Protect&trade; Cold is a 100% natural sea water based solution and is free from steroids, drugs and preservatives, high in marine trace elements and enriched with copper. Our solution has a salt concentration higher than that of the body cells, enabling relieving nasal congestion naturally and actively ("osmotic&nbsp;effect").</p>
					</li>
					<li>
						@include('main.products.partials._expand-collapse')
						<h5>Why choose ST&Eacute;RIMAR&trade; Baby Stop &amp; Protect&trade; Cold?</h5>
						<p>Based on ingredients of natural origin, our solution acts rapidly and effectively on cold symptoms fighting against their development and helping the baby to breathe easier. It is recommended for babies from 3 months and&nbsp;up.</p>
					</li>
				</ul>
			</div>
		</div>

		<div class="columns span-3 sm-lg-7 col4 eq hidden vis-sm-lg">
			<div class="wrap down eq-inner">
				
				<img src="/images/products/baby-stop-and-protect.png" class="laze"/>
			</div>
		</div>

		<div class="columns span-3 sm-lg-5 col3 eq" group="g1">
			<div class="wrap eq-inner">
				<!-- allergyUK image /public/images/allergy-uk.svg -->
				@include('main.products.partials._side-links')

			</div>
		</div>
	</div>

	<div class="row full section alt blue interested">
		<div class="columns col span-12">
			<h1>You may also be interested in</h1>
		</div>
		<div class="row threecol">
			<div class="columns">
				<a href="/our-products/nasal-hygiene-breathe-easy-daily"><img src="/images/products/alt-nasal-hygiene.png" class="packs"/></a>
				<h3>Nasal Hygiene</h3>
			</div>
			<div class="columns">
				<a href="/our-products/allergy"><img src="/images/products/alt-allergy.png" class="packs"/></a>
				<h3>Allergy Relief</h3>
			</div>
			<div class="columns">
				<a href="/our-products/cold-and-sinus"><img src="/images/products/alt-cold-and-sinus.png" class="packs"/></a>
				<h3>Cold and Sinus</h3>
			</div>
		</div>
	</div>

	@include('main.products.partials._not-sure')

@endsection

@section('footer')
	@include('main.layouts.partials._footer', ['footnote' => '*GPRX data: Sodium Chloride sub-category: data from April 2016 to April 2018.'])
@endsection
