@extends('main.layouts.main')


@section('top')

@endsection


@section('content')

	<div class="productHomeHero">
		<div class="banner">
			<h1>THE WHOLE ST&Eacute;RIMAR FAMILY</h1>
			<p>Learn more about our range of effective and drug-free nasal spray solutions inspired by nature.</p>
		</div>
		<div class="frame"><img alt="" src="/images/products/header-range-alt.jpg"></div>
		<p class="disclaimer">GPrX data: Sodium Chloride sub-category: data from July 2019 to June 2020</p>
		<div class="bar">
			<p>Scroll down to<br>discover our range</p>
		</div>
	</div>

	@if ($isWinter)
		@include('main.products.partials._range_nasal_hygiene')
		@include('main.products.partials._range_decongestant')
		@include('main.products.partials._range_allergy')
		@include('main.products.partials._range_children')
	@else
		@include('main.products.partials._range_nasal_hygiene')
		@include('main.products.partials._range_allergy')
		@include('main.products.partials._range_decongestant')
		@include('main.products.partials._range_children')
	@endif

	@include('main.products.partials._not-sure')

@endsection

@section('footer')
	@include('main.layouts.partials._footer', ['footnote' => ''])
@endsection
