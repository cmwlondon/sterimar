@extends('main.layouts.main')


@section('top')

@endsection


@section('content')

  <div class="row full">
    <div class="columns span-7 md-12 sm-12 rel">
      <img src="/images/contact/phone.jpg" class="behind" alt="" />
      <div class="over">
        <h1>Contact Us</h1>
        <p>Looking for more information on St&eacute;rimar products? Please contact our customer careline below. Whatever you need to know, our trained advisors are here to help you.</p>
      </div>

    </div>
    <div class="general-enquiries columns span-4 before-1 md-10 md-before-1 md-after-1  sm-10 sm-before-1 sm-after-1">
      <h3>General Enquiries</h3>
      <p>Telephone: 0800 121 6080</p>
        <p>If you’d like to contact us by email, please fill in the form <a href="#contactForm">below</a></p>
    </div>
  </div>
  <div class="row full">
    <div class="columns span-12">
      <hr class="cf w100 mt3"/>
    </div>
  </div>
  <div class="contact row full" id="contactForm">
    <div class="form-wrap columns span-8 before-2 after-2 md-10 md-before-1 md-after-1  sm-10 sm-before-1 sm-after-1">
      <h3>Your Feedback</h3>
      <p class="mt1">We would welcome your feedback about St&eacute;rimar. We are committed to continual innovation and improvements to our products and so we would love to hear from you. Please take a moment to complete our simple questionnaire. We are not asking for any personal data and we will only use the information provided to help us improve St&eacute;rimar and to aid us in making the product more readily available.</p>
    </div>
  </div>
  <div class="contact row full">
    <div class="form-wrap columns span-8 before-2 after-2 sm-10 sm-before-1 sm-after-1">

      @if (session('response'))
        <div class="form-row row full">
          <div class="columns span-8 medium-12 small-12 response">
            <h3>{{ session('response.message') }}</h3>
          </div>
        </div>
      @else

        {!! Form::open(array('url' => ['contact#form'], 'method' => 'POST', 'id' => 'supportForm') )!!}

          <div class="form-row row full">
            <div class="columns span-4 sm-12 md-12">
              {!! Form::label('title', 'Title: *') !!}
            </div>
            <div class="columns span-8 sm-12 md-12">
              {!! Form::select('title', $titlesArr, null, ['id' => 'title', 'class' => 'input short'] ) !!}
            </div>
            <div class="columns span-8 before-4 md-12 md-before-0 sm-12 sm-before-0">
              {!! $errors->first('title', '<small class="error">:message</small>') !!}
            </div>
          </div>

          <div class="form-row row full">
            <div class="columns span-4 sm-12 md-12">
              {!! Form::label('firstname', 'First Name: *') !!}
            </div>
            <div class="columns span-8 sm-12 md-12">
              {!! Form::text('firstname',null,['placeholder' => '', 'id' => 'firstname', 'class' => 'input']) !!}
            </div>
            <div class="columns span-8 before-4 md-12 md-before-0 sm-12 sm-before-0">
              {!! $errors->first('firstname', '<small class="error">:message</small>') !!}
            </div>
          </div>

          <div class="form-row row full">
            <div class="columns span-4 sm-12 md-12">
              {!! Form::label('lastname', 'Last Name: *') !!}
            </div>
            <div class="columns span-8 sm-12 md-12">
              {!! Form::text('lastname',null,['placeholder' => '', 'id' => 'lastname', 'class' => 'input']) !!}
            </div>
            <div class="columns span-8 before-4 md-12 md-before-0 sm-12 sm-before-0">
              {!! $errors->first('lastname', '<small class="error">:message</small>') !!}
            </div>
          </div>
          <?php /*
          <div class="form-row row full">
            <div class="columns span-4 sm-12 md-12">
              {!! Form::label('address', 'Address:') !!}
            </div>
            <div class="columns span-8 sm-12 md-12">
              {!! Form::text('address',null,['placeholder' => '', 'id' => 'address', 'class' => 'input']) !!}
            </div>
            <div class="columns span-8 before-4 md-12 md-before-0 sm-12 sm-before-0">
              {!! $errors->first('address', '<small class="error">:message</small>') !!}
            </div>
          </div>

          <div class="form-row row full">
            <div class="columns span-4 sm-12 md-12">
              {!! Form::label('city', 'City:') !!}
            </div>
            <div class="columns span-8 sm-12 md-12">
              {!! Form::text('city',null,['placeholder' => '', 'id' => 'city', 'class' => 'input']) !!}
            </div>
            <div class="columns span-8 before-4 md-12 md-before-0 sm-12 sm-before-0">
              {!! $errors->first('city', '<small class="error">:message</small>') !!}
            </div>
          </div>

          <div class="form-row row full">
            <div class="columns span-4 sm-12 md-12">
              {!! Form::label('postcode', 'Postcode:') !!}
            </div>
            <div class="columns span-8 sm-12 md-12">
              {!! Form::text('postcode',null,['placeholder' => '', 'id' => 'postcode', 'class' => 'input']) !!}
            </div>
            <div class="columns span-8 before-4 md-12 md-before-0 sm-12 sm-before-0">
              {!! $errors->first('postcode', '<small class="error">:message</small>') !!}
            </div>
          </div>

          <div class="form-row row full">
            <div class="columns span-4 sm-12 md-12">
              {!! Form::label('country', 'Country:') !!}
            </div>
            <div class="columns span-8 sm-12 md-12">
              {!! Form::text('country',null,['placeholder' => '', 'id' => 'country', 'class' => 'input']) !!}
            </div>
            <div class="columns span-8 before-4 md-12 md-before-0 sm-12 sm-before-0">
              {!! $errors->first('country', '<small class="error">:message</small>') !!}
            </div>
          </div>
          */ ?>

          <div class="form-row row full">
            <div class="columns span-4 sm-12 md-12">
              {!! Form::label('phone', 'Phone Number:') !!}
            </div>
            <div class="form columns span-8 sm-12 md-12">
              {!! Form::text('phone',null,['placeholder' => '', 'id' => 'phone', 'class' => 'input']) !!}
            </div>
            <div class="columns span-8 before-4 md-12 md-before-0 sm-12 sm-before-0">
              {!! $errors->first('phone', '<small class="error">:message</small>') !!}
            </div>
          </div>

          <div class="form-row row full">
            <div class="columns span-4 sm-12 md-12">
              {!! Form::label('email', 'Email Address: *') !!}
            </div>
            <div class="columns span-8 sm-12 md-12">
              {!! Form::text('email',null,['placeholder' => '', 'id' => 'email', 'class' => 'input']) !!}
            </div>
            <div class="columns span-8 before-4 md-12 md-before-0 sm-12 sm-before-0">
              {!! $errors->first('email', '<small class="error">:message</small>') !!}
            </div>
          </div>

          <div class="form-row row full">
            <div class="columns span-4 sm-12 md-12">
              {!! Form::label('product', 'Product:') !!}
            </div>
            <div class="columns span-8 sm-12 md-12">
              {!! Form::text('product',null,['placeholder' => '', 'id' => 'product', 'class' => 'input']) !!}
            </div>
            <div class="columns span-8 before-4 md-12 md-before-0 sm-12 sm-before-0">
              {!! $errors->first('product', '<small class="error">:message</small>') !!}
            </div>
          </div>

          <div class="form-row row full">
            <div class="columns span-4 sm-12 md-12">
              {!! Form::label('comments', 'Comments:') !!}
            </div>
            <div class="columns span-8 sm-12 md-12">
              {!! Form::textarea('comments',null,['placeholder' => '', 'id' => 'comments', 'rows' => '4', 'class' => 'textarea-input']) !!}
            </div>
            <div class="columns span-8 before-4 md-12 md-before-0 sm-12 sm-before-0">
              {!! $errors->first('comments', '<small class="error">:message</small>') !!}
            </div>
          </div>

          <div class="form-row row full">
            <div class="columns span-12 sm-12 md-12 sm-before-0 md-before-0">
              <a href="#" class="submit-a" onclick="$(this).closest('form').submit()"><i class="demo-icon icon-submit"></i></a>
            </div>
          </div>

          <div class="form-row row full">
						<div class="columns span-8 before-4 sm-12 md-12 sm-before-0 md-before-0">
							<a href="#" class="submit-form" onclick="$(this).closest('form').submit()">Submit</i></a>
						</div>
					</div>

        {!! Form::close() !!}

      @endif
    </div>
  </div>

  @include('main.products.partials._not-sure')

@endsection

@section('footer')
	@include('main.layouts.partials._footer', ['footnote' => '*GPRX data: Sodium Chloride sub-category: data from April 2016 to April 2018.'])
@endsection
