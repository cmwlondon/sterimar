@extends('main.layouts.main')


@section('top')

@endsection


@section('content')
	<div class="carousel-header product-style">
		<div class="bg" style="background-image: url('/images/nasal-health/stethoscope-heading.jpg');">

			<div class="text">
				<h1>What is<br/><span>nasal hygiene?</span></h1>
				<p>It's only your nose, so why nasal hygiene?</p>
			</div>

			<div class="bar">
				<div class="row full">
					<div class="columns span-6 splitL">
						<span>Scroll down to<br>find out more</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="testimonial blue">
		<a name="information"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'information'])
		<img src="/images/quote-left.svg" class="ql"/>
		<img src="/images/quote-right.svg" class="qr"/>
		@include('main.products.partials._page-quotes', ['quotCount' => '1', 'quotRef' => [3]])
	</div>


	<div class="row section what-is">
		<div class="columns span-9 sm-lg-12">
			<ul class="info">
				<li class="active">
					@include('main.products.partials._expand-collapse')
					<h5>The nose is your first line of defense.</h5>
					<p>The nose is a highly effective filter for your body. Filtering through over 15,000 litres of air a day and wonderful smells such as flowers or a fresh coffee - whilst doing its best to filter out the bad, all that dust, pollution and allergens, looking to enter into your body through your nasal passages. Once trapped, the tiny hairs along the lining of the nasal passages called cilia work to carry away the impurities to the back of the throat where they are swallowed and destroyed before they have a chance to multiply and cause harm.</p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>Help your nose defend against airborne allergens and germs. </h5>
					<p>If the nasal passage (the inside of your nose) dries out or is clogged, it cannot be as efficient when performing its crucial role in keeping you healthy. That is where nasal hygiene comes in. Ensuring that the nose is clean, clear and moisturised can help ensure the nose remains in top working condition to help defend against the airborne contaminants you breathe in. It helps to prevent the onset of symptoms and reduce the risk of further problems occurring.</p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>So nasal hygiene isn&rsquo;t a vanity - it actually helps you stay well and keep breathing?</h5>
					<p>Yeah that&rsquo;s right. Keeping your nose clean improves defences against cold symptoms, rhinopharyngitis and hayfever. To protect the body and do its job properly the nose needs to stay in top condition, that means keeping it clean and clear.</p>
				</li>
			</ul>
		</div>

		<div class="columns span-3 sm-lg-11 sm-lg-before-1 learn-more">
			{{-- <img src="/images/allergy-uk.svg" class="allergyuk"/> --}}
			@include('main.products.partials._side-links')

		</div>
	</div>


	@include('main.products.partials._not-sure')

@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
