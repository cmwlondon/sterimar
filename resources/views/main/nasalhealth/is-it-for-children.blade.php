@extends('main.layouts.main')


@section('top')

@endsection


@section('content')
	<div class="carousel-header product-style">
		<div class="bg" style="background-image: url('/images/nasal-health/is-it-for-children-heading.jpg');">

			<div class="text">
				<h1>Is it for<br/><span>children?</span></h1>
				<p>What about my kids or newborn baby?</p>
			</div>

			<div class="bar">
				<div class="row full">
					<div class="columns span-6 splitL">
						<span>Scroll down to<br>find out more</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="testimonial blue">
		<a name="information"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'information'])
		<img src="/images/quote-left.svg" class="ql"/>
		<img src="/images/quote-right.svg" class="qr"/>
		@include('main.products.partials._page-quotes', ['quotCount' => '1', 'quotRef' => [2]])
	</div>


	<div class="row section what-is">
		<div class="columns span-9 sm-lg-12">
			<ul class="info">
				<li class="active">
					@include('main.products.partials._expand-collapse')
					<h5>Doesn&rsquo;t it just seem like children are more likely to catch a cold?</h5>
					<p>Well, they are. Kids&rsquo; immune systems haven&rsquo;t had as long to develop defences against the germs that cause colds - and all those runny noses sharing a classroom or play group make it all the more likely that germs will spread. As with adults, ensuring the nose is kept clean and clear can help it do its natural filtering job most effectively to ensure this first line of defence is in top working condition to make sure it&rsquo;s even gentle on tiny, little noses.</p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>The importance of keeping your little ones nose clean and clear.</h5>
					<p>To make sure your baby's nasal hygiene is tiptop you should start caring for their nasal hygiene daily from birth. Once your child is old enough to blow their own nose you should teach them how to keep their nasal passage clean and clear, allowing them to breathe naturally and helping them to stay healthy as they grow.</p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>Safe and effective nasal relief for the whole family!</h5>
					<p>ST&Eacute;RIMAR<sup>TM</sup>is the UK’s No.1 recommended non-medicated nasal spray brand*. It is completely drug, steroid and preservative free to make sure it is gentle on even tiny little noses. The 100% natural, purified sea water used in all ST&Eacute;RIMAR<sup>TM</sup>products undergoes rigorous controlled testing to ensure its purity is of the highest quality and the very gentle micro-diffusion spray is specifically suited for children&rsquo;s sensitive nasal mucosa**. For these reasons it is no wonder the ST&Eacute;RIMAR<sup>TM</sup>range is trusted and recommended by thousands of healthcare professionals and parents across the UK.
            <br><br><span class="disclaimer">*GPRX data: Sodium Chloride sub-category: data from July 2019 to June 2020.</span>
            <br><br><span class="disclaimer">**ST&Eacute;RIMAR<sup>TM</sup>baby can be used from birth - ST&Eacute;RIMAR<sup>TM</sup>kids can be used from 3 months.</span></p>
				</li>
			</ul>
		</div>

		<div class="columns span-3 sm-lg-11 sm-lg-before-1 learn-more">
			{{-- <img src="/images/allergy-uk.svg" class="allergyuk"/> --}}
			@include('main.products.partials._side-links')

		</div>
	</div>


	@include('main.products.partials._not-sure')

@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
