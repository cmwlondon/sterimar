@extends('main.layouts.main')


@section('top')

@endsection


@section('content')
	<div class="carousel-header intro">
		<div class="bg" style="background-image: url('/images/nasal-health/stethoscope-heading.jpg');">
			
			<div class="text-heading">
				<h1>What is <span>nasal hygiene?</span></h1>
			</div>
			<div class="text-extra">
				<p>Ensuring that your nose is clean, clear and moisturised can help ensure your nose is in top working condition to help defend against the airborne contaminants you breathe in.<br/><br/><a href="/what-is-nasal-hygiene">read more</a></p>
			</div>

		</div>
	</div>
	

	<div class="row section section-1">
		<a name="options"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'options'])
		
		<div class="columns span-6 md-12 sm-12 spaced">
			<div class="img-area">
				<a href="/why-sterimar">
					<h2>Why <span>St&eacute;rimar?</span></h2>
					<img src="/images/nasal-health/why-sterimar.jpg" alt="Why St&eacute;rimar?"/>
				</a>
			</div>
			<p>St&eacute;rimar solutions are scientifically proven to effectively relieve symptoms without the potential side effects associated with medicated treatments... <a href="/why-sterimar">read more</a></p>
		</div>

		<div class="columns span-6 md-12 sm-12 spaced">
			<div class="img-area">
				<a href="/is-it-for-children">
					<h2>Is it for <span>children?</span></h2>
					<img src="/images/nasal-health/is-it-for-children.jpg" alt="Is it for children?"/>
				</a>
			</div>
			<p>The St&eacute;rimar children range is specifically designed to gently clear and unblock little noses keeping them in top condition to help prevent colds and aid natural breathing... <a href="/is-it-for-children">read more</a></p>
		</div>

		<div class="columns span-6 md-12 sm-12">
			<div class="img-area">
				<a href="/how-to-use">
					<h2>How to use it</span></h2>
					<img src="/images/nasal-health/how-to-use-it.jpg" alt="How to use it"/>
				</a>
			</div>
			<p>Find out how to use St&eacute;rimar for adults and children... <a href="/how-to-use">read more</a></p>
		</div>

		<div class="columns span-6 md-12 sm-12">
			<div class="img-area">
				<a href="/faqs">
					<h2>Faqs</span></h2>
					<img src="/images/nasal-health/faqs.jpg" alt="Faqs"/>
				</a>
			</div>
			<p>Read our frequently asked questions to find out more about our St&eacute;rimar solutions... <a href="/faqs">read more</a></p>
		</div>
	</div>

	
	@include('main.products.partials._not-sure')

@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
