@extends('main.layouts.main')


@section('top')

@endsection


@section('content')
	<div class="carousel-header product-style">
		<div class="bg" style="background-image: url('/images/nasal-health/why-sterimar-heading.jpg');">

			<div class="text">
				<h1>Why<br/><span>St&eacute;rimar?</span></h1>
				<p>Ok, nasal hygiene is good, but why ST&Eacute;RIMAR<sup>TM</sup>?</p>
			</div>

			<div class="bar">
				<div class="row full">
					<div class="columns span-6 splitL">
						<span>Scroll down to<br>find out more</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="testimonial blue">
		<a name="information"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'information'])
		<img src="/images/quote-left.svg" class="ql"/>
		<img src="/images/quote-right.svg" class="qr"/>
		@include('main.products.partials._page-quotes', ['quotCount' => '1', 'quotRef' => [1]])
	</div>

	<div class="row section what-is">
		<div class="columns span-9 sm-lg-12">
			<ul class="info">
				<li class="active">
					@include('main.products.partials._expand-collapse')
					<h5>ST&Eacute;RIMAR<sup>TM</sup> effectively relieves symptoms without the compromise of steroids, drugs and preservatives that can be found in other more medicated products.</h5>
					<p>Our 100% natural, purified seawater based nasal sprays are completely drug and preservative free. This means that the ST&Eacute;RIMAR<sup>TM</sup> product range offers nasal solutions for everyone, even babies* and pregnant women or breastfeeding mothers.
					<br/><br/>
					<span class="disclaimer">*ST&Eacute;RIMAR<sup>TM</sup> Baby can be used from birth and ST&Eacute;RIMAR<sup>TM</sup> Kids can be used from 3 months. ST&Eacute;RIMAR<sup>TM</sup> Hayfever & Allergies, Congestion Relief and Stop &amp;Protect range can be used from 3 years &amp;up.</span></p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>Over 40 years of expertise in nasal health.</h5>
					<p>ST&Eacute;RIMAR<sup>TM</sup> has a strong heritage in providing natural based remedies for nasal conditions and is the UK’s No.1 recommended non-medicated nasal spray brand*. Using the expertise of healthcare professionals and early childhood specialists, ST&Eacute;RIMAR<sup>TM</sup> range of solutions has been developed to <a href="/our-products/allergy#nasal-hygiene">effectively prevent</a> or treat  nasal conditions <a href="/our-products/allergy#stop-and-protect">associated with allergies</a>, <a href="/our-products/cold-and-sinus#congestion-relief">congestion</a> and <a href="/our-products/cold-and-sinus#stop-and-protect">blocked noses</a> when most needed. The ST&Eacute;RIMAR<sup>TM</sup> range is an effective solution trusted by many leading healthcare professionals in the UK, and used by millions around the world.
					<!-- <br><br>*GPRX data: Add source - UK coverage. -->
					<br><br>
					<span class="disclaimer">*GPrX data: non-medicated nasal sprays, UK, from July 2019 to June 2020.</span></p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>Pure sea water is rich in different salts, minerals and trace elements that are important for overall health and wellbeing.</h5>
					<p>ST&Eacute;RIMAR<sup>TM</sup> solutions are inspired by nature and made from specially monitored sea water, found off the French coast of Brittany.</p>
				</li>
			</ul>
		</div>

		<div class="columns span-3 sm-lg-11 sm-lg-before-1 learn-more">
			{{-- <img src="/images/allergy-uk.svg" class="allergyuk"/> --}}
			<img alt="No.1 GP recommended brand*" src="/images/gp-recommended-brand2.png" class="gp-recommended-brand" />
			@include('main.products.partials._side-links')

		</div>
	</div>

	@include('main.products.partials._not-sure')

@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
