@extends('main.layouts.main')


@section('top')

@endsection


@section('content')
	<div class="carousel-header product-style">
		<div class="bg" style="background-image: url('/images/nasal-health/how-to-use-heading.jpg');">

			<div class="text">
				<h1>How to use it?</h1>
				<p>Instructions for use for adults, children and babies.</p>
			</div>

			<div class="bar">
				<div class="row full">
					<div class="columns span-6 splitL">
						<span>Scroll down to<br>find out more</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="testimonial blue">
		<a name="information"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'information'])
		<img src="/images/quote-left.svg" class="ql"/>
		<img src="/images/quote-right.svg" class="qr"/>
		@include('main.products.partials._page-quotes', ['quotCount' => '1', 'quotRef' => [3]])
	</div>


	<div class="row section what-is">
		<div class="columns span-8 after-1 sm-lg-12">

			<?php /*
			<div class="row">
				<div class="columns span-6 lg-12 md-12 sm-12 video">
					<div class="row full">
						<div class="columns span-12">
							<h2><span>How to use</span> St&eacute;rimar for adults and children</h2>
						</div>
						<div class="columns span-12">
							<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAMAAAAM9FwAAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRF////AAAAVcLTfgAAAAF0Uk5TAEDm2GYAAAAOSURBVHjaYmAYpAAgwAAAmQABh704YAAAAABJRU5ErkJggg==" class="shim"/>
							<iframe width="100%" height="100%" src="https://www.youtube.com/embed/5VBdP4sk88I" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="columns span-12 mt1">
							<a href="/download/instructions/adults-and-children" class="pdf-link">Click here to download the PDF instructions &gt;</a>
						</div>
					</div>
				</div>
				<div class="columns span-6 lg-12 md-12 sm-12 video">
					<div class="row full">
						<div class="columns span-12">
							<h2><span>How to use</span> St&eacute;rimar for babies</h2>
						</div>
						<div class="columns span-12">
							<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAMAAAAM9FwAAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRF////AAAAVcLTfgAAAAF0Uk5TAEDm2GYAAAAOSURBVHjaYmAYpAAgwAAAmQABh704YAAAAABJRU5ErkJggg==" class="shim"/>
							<iframe width="100%" height="100%" src="https://www.youtube.com/embed/UF4Q6XU2HaI" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="columns span-12 mt1">
							<a href="/download/instructions/babies" class="pdf-link">Click here to download the PDF instructions &gt;</a>
						</div>
					</div>
				</div>
			</div>
			*/ ?>

			<div class="row">
				<div class="columns span-12">
					<h2><span>How to use</span> St&eacute;rimar for adults and kids</h2>
					<p>St&eacute;rimar has a unique nozzle that disperses the solution in very fine droplets further up the nose for increased efficacy. St&eacute;rimar is easy to use at any angle.</p>
					<ol>
						<li>Prime the spray by briefly pressing the nozzle.</li>
						<li>Insert the nozzle into one nostril.</li>
						<li>Press briefly without tilting your or your head backwards or sniffing.</li>
						<li>Blow your nose.</li>
						<li>Repeat in other nostril.</li>
					</ol>
					<p>Clean the nozzle after application with soapy water, rinse and dry.</p>
					<p>Use as often as required or as recommended by your doctor.</p>

				</div>

				<div class="columns span-12 mb4">
					<h2><span>How to use</span> St&eacute;rimar for babies</h2>
					<p>St&eacute;rimar has a unique nozzle specifically designed to clear mucus and impurities further up the nose for a better yet gentle clearing effect. St&eacute;rimar Baby is easy to use at any angle.</p>
					<ol>
						<li>Prime the spray by briefly by depressing the nozzle.</li>
						<li>Lay baby down on his/her side or sit up with baby&rsquo;s head tilted to one side.</li>
						<li>Gently insert the nozzle into the nostril.</li>
						<li>Press the nozzle briefly &ndash; let any excess solution run out before wiping the nose.</li>
						<li>Repeat in the other nostril.</li>
					</ol>
					<p>Clean the nozzle after application with soapy water, rinse and dry.</p>
					<p>Use as often as required or as recommended by your doctor.</p>


				</div>

			</div>

		</div>

		<div class="columns span-3 sm-lg-11 sm-lg-before-1 learn-more">
			{{-- <img src="/images/allergy-uk.svg" class="allergyuk"/> --}}
			@include('main.products.partials._side-links')

		</div>
	</div>

	@include('main.products.partials._not-sure')

@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
