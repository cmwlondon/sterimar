@extends('main.layouts.main')


@section('top')

@endsection


@section('content')
	<div class="carousel-header product-style">
		<div class="bg" style="background-image: url('/images/nasal-health/faqs-heading.jpg');">

			<div class="text">
				<h1>Faqs</span></h1>
				<p>Read our frequently asked questions to find out more about our St&eacute;rimar solutions.</p>
			</div>

			<div class="bar">
				<div class="row full">
					<div class="columns span-6 splitL">
						<span>Scroll down to<br>find out more</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="testimonial blue">
		<a name="information"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'information'])
		<img src="/images/quote-left.svg" class="ql"/>
		<img src="/images/quote-right.svg" class="qr"/>
		@include('main.products.partials._page-quotes', ['quotCount' => '1', 'quotRef' => [5]])
	</div>


	<div class="row section what-is">
		<div class="columns span-9 sm-lg-12">
			<ul class="info">
				<li class="active">
					@include('main.products.partials._expand-collapse')
					<h5>How often should I use St&eacute;rimar?</h2>
					<p>We recommend using St&eacute;rimar Isotonic and St&eacute;rimar Baby two to six times a day (or more) in each nostril for as long as you need it or as long as and as often as your doctor recommends.</p>
					<p>St&eacute;rimar Hypertonic is recommended for a short period. Use two to four times a day in each nostril for light congestion and up to six times a day for severe congestion.</p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>Can St&eacute;rimar be used with other medication?</h2>
					<p>Yes. St&eacute;rimar is drug-free and doesn’t contain preservatives so there is no known risk of interaction with any other medication.  However, in all cases it is best to check with your doctor.</p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>Am I likely to experience any irritation from using St&eacute;rimar?</h2>
					<p>St&eacute;rimar Isotonic and St&eacute;rimar Baby are purified sea water solutions with the same concentration of salts as the cells in the human body.  As such, irritation is highly unlikely.  St&eacute;rimar Hypertonic has a higher salt content than St&eacute;rimar Isotonic but the solution is still less salty than sea water.  A sensation of dryness may occur and should disappear within a few days.</p>
					<p>Always follow instructions and/or doctor's recommendations.</p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>Can I use St&eacute;rimar if I am pregnant, diabetic or suffer from high blood pressure?</h2>
					<p>Yes, St&eacute;rimar is completely safe to use with these conditions, but it is always a good idea to check with your doctor.</p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>Can I use St&eacute;rimar for my three year old?</h2>
					<p>Yes, St&eacute;rimar Isotonic is suitable for the whole of the family, even babies from 3 months old.  St&eacute;rimar Hypertonic is suitable for all adults and children over three years of age and St&eacute;rimar Baby is a new part of the range and can be used with babies from birth to 3 years old.</p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>Is the nozzle safe to use?</h2>
					<p>Yes. The St&eacute;rimar nozzle has been specifically designed to fit any shape and size of nostrils.</p>
					<p>St&eacute;rimar Baby has a specific nozzle with a protective flange. The nozzle cannot be pushed too far into the nose to avoid any risks of injury.</p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>How long will St&eacute;rimar keep?</h2>
					<p>St&eacute;rimar products can be kept for a maximum of three years after manufacture or until all solution has been used.  The expiry date can be found at the bottom of the can and carton. Each dose of St&eacute;rimar delivered is aseptic.  The valve on top of the can prevents air from entering. Also, because the solution is kept in a sterilised pouch it never comes into contact with either the aluminium can or the non-flammable nitrogen gas within.</p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>Why is it important to clean the nozzle after each use?</h2>
					<p>You must clean the nozzle in soapy water to remove any microorganisms left after use to help avoid any cross-contamination.</p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>What does micro-diffusion mean?</h2>
					<p>The unique St&eacute;rimar micro-diffusion system produces a fine spray of sea water.  As the droplets produced by St&eacute;rimar are microfine, they remain in contact with the walls of the nasal passages for longer, making St&eacute;rimar even more effective.</p>
				</li>
				<li>
					@include('main.products.partials._expand-collapse')
					<h5>Why are the copper and manganese in St&eacute;rimar Hypertonic important?</h2>
					<p>Copper and manganese are found in very small amounts in the human body.  They stimulate the body’s self defense mechanisms and help keep it healthy.</p>
				</li>
			</ul>
		</div>

		<div class="columns span-3 sm-lg-11 sm-lg-before-1 learn-more">
			{{-- <img src="/images/allergy-uk.svg" class="allergyuk"/> --}}
			@include('main.products.partials._side-links')

		</div>
	</div>


	@include('main.products.partials._not-sure')

@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
