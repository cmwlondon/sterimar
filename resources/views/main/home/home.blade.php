@extends('main.layouts.main')


@section('top')
{{--
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.10&appId={{{$appID}}}";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
--}}
@endsection


@section('content')
	<div class="home-hero">
		<a href="/our-products/nasal-hygiene#breathe-easy-daily" title="HELPING YOU BREATHE BETTER, DAY AND NIGHT - Most Valued Product 2020 award winner
STERIMAR BREATHE EASY DAILY - CLINICALLY PROVEN"><img alt="HELPING YOU BREATHE BETTER, DAY AND NIGHT - Most Valued Product 2020 award winner
STERIMAR BREATHE EASY DAILY - CLINICALLY PROVEN" src="images/home/header.jpg"></a>
	</div>

	<div class="row section-1">
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'allergy-sufferer'])

		<div class="headerTrail blue">
			<div><p>SCROLL DOWN TO DISCOVER <br>OUR STORY AND ADVERTS</p></div>
		</div>

		<a name="allergy-sufferer"></a>
		<div class="columns span-4 sm-lg-12">
			<a href="/our-products/allergy">
			<h2><span>Allergy</span> Sufferer?</h2>
			<div class="img">
				<img src="/images/home/block-1.jpg" alt=""/>
				<h3></h3>
			</div>
			<div class="synopsis">
				<img src="/images/home/pack-allergy-response.png" class="pack animated fadeIn" data-id="1">
				<p>Rapid and effective relief from allergies. <span>Read more</span></p>
			</div>
			</a>
		</div>
		<div class="columns span-4 sm-lg-12 blocked-nose">
			<a href="/our-products/cold-and-sinus">
			<h2><span>Blocked</span> Nose?</h2>
			<div class="img">
				<img src="/images/home/block-2.jpg" alt=""/>
				<h3></h3>
			</div>
			<div class="synopsis">
				<img src="/images/home/pack-cold-and-sinus-relief.png" class="pack animated fadeIn" data-id="2">
				<p>Relief to decongest nasal passages. <span>Read more</span></p>
			</div>
			</a>
		</div>
		<div class="columns span-4 sm-lg-12">
			<a href="/our-products/baby-and-kids">
			<h2><span>Is it for</span> children?</h2>
			<div class="img">
				<img src="/images/home/block-3.jpg" alt=""/>
				<h3></h3>
			</div>
			<div class="synopsis">
				<img src="/images/home/pack-toddlers-and-kids.png" class="pack animated fadeIn" data-id="3">
				<p>Naturally clears and unblocks little noses. <span>Read more</span></p>
			</div>
			</a>
		</div>
	</div>

	<div class="row admodule0">
		<h2><span>WATCH</span> OUR ADVERTS</h2>
	</div>
	<div class="row admodule1">
		<div class="columns span-4 sm-lg-12">
			<h3 class="vid03"><span>STERIMAR</span> HAYFEVER &amp; ALLERGY RELIEF</h3>
			<div class="videoPlay" data-video="/video/home/hayfever-allergies-2kbps.mp4" data-poster="/images/home/poster3.jpg" data-caption="STERIMAR HAYFEVER &amp; ALLERGY RELIEF">
				<a href="#" data-trigger="ad03" class="blueplay videoPopupTrigger"></a>
				<img src="/images/home/video3.jpg" alt=""/>
			</div>
			</a>
		</div>
		<div class="columns span-4 sm-lg-12">
			<h3 class="vid01"><span>STERIMAR</span> CONGESTION RELIEF</h3>
			<div class="videoPlay" data-video="/video/home/congestion-relief-q4-2020.mp4" data-poster="/images/home/poster1.jpg" data-caption="STERIMAR CONGESTION RELIEF">
				<a href="#" data-trigger="ad01" class="blueplay videoPopupTrigger"></a>
				<img src="/images/home/video1.jpg" alt=""/>
			</div>
			</a>
		</div>
		<div class="columns span-4 sm-lg-12">
			<h3 class="vid02"><span>STERIMAR</span> BREATHE EASY DAILY</h3>
			<div class="videoPlay" data-video="/video/home/breathe-easy-daily.mp4" data-poster="/images/home/poster2.jpg" data-caption="STERIMAR BREATHE EASY DAILY">
				<a href="#" data-trigger="ad02" class="blueplay videoPopupTrigger"></a>
				<img src="/images/home/video2.jpg" alt=""/>
			</div>
			</a>
		</div>
	</div>

	<div class="row TVCTitle">
		<h2><span>DISCOVER</span> OUR BRAND STORY</h2>
	</div>
	<div class="row full section-2 TVCWrapper">
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'tvad'])
		<a name="tvad"></a>
			<div class="tvc waiting" data-mpfour="/video/home/brand-video.mp4" data-webm="" data-ogv="">
			<div class="overlay"></div>
		</div>
	</div>

	<div class="row full section-2">
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'nasal-health'])
		<a name="nasal-health"></a>
		<div class="carousel-wrap">
			<div class="item">
				<div class="content">
					<h2><span>What is</span> nasal hygiene?</h2>
					<blockquote>
						<p><img src="/images/quote-left.svg" class="quote left"/> St&eacute;rimar Breathe Easy Daily sea water nasal spray not only washes away dust and allergens from the nose, to ease symptoms, but also helps to restore the nose&rsquo;s natural functions. <img src="/images/quote-right.svg" class="quote right"/></p>
						<footer>
							<cite>Dr Rudenko, Medical Director of the London Allergy and Immunology Centre</cite>
						</footer>
					</blockquote>
					<p><a href="/what-is-nasal-hygiene" class="btn">learn more</a></p>
				</div>
			</div>
		</div>
	</div>

	<div class="row full section-3">
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'news', 'extra' => '<div class="bar"></div>'])
		<a name="news"></a>
		<div class="news-wrap">
			<div class="row full rel">
				<div class="grad"></div>
				<div class="columns span-12 lg-12 md-12 sm-12 news">
					<div class="inner">
						<h2><span>Latest</span> News</h2>
						<ul>
							@foreach ($news as $post)
							<li>
								<a href="/news/{{{$post->slug}}}"><div style="background-image:url(/uploads/blog/thumbs/{{{$post->thumb}}});" class="icon"></div>
								<div class="headline">
									<h3>{{{$post->title}}}</h3>
									<h5>{!!$post->created_at->format('jS \\of F')!!}</h5>
									<p>{{{$post->synopsis}}}</p>
									<p class="more">read more</p>
								</div></a>
							</li>
							@endforeach
						</ul>
					</div>
				</div>
				<div class="columns span-6 lg-12 md-12 sm-12 facebook">
					<div class="inner">
						{{--
						<h2><span>Follow us</span> on Facebook</h2>
						<div class="fb-page" data-href="https://www.facebook.com/sterimarUK/" data-tabs="timeline" data-width="500" data-height="70" data-small-header="false" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"></div>
						--}}
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="videopopup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">

	      <div class="modal-header">
	        <h5 class="modal-title" id="videoCaption">Modal title</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">×</span>
	        </button>
	      </div>

	      <div class="modal-body">
	        {{-- <video preload="metadata" controls="controls" playsinline="" class="waiting"><source type="video/mp4" src="/video/home/brand-video.mp4"></video> --}}
	      </div>

	      {{-- <div class="modal-footer"></div> --}}
	    </div>
	  </div>
	</div>

	@include('main.products.partials._not-sure')

@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
