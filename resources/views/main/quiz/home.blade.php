@extends('main.layouts.main')


@section('top')

@endsection



@section('content')

	<div class="carousel-header">
		<div class="bg" style="background-image: url('/images/products/header-home.jpg');">
			<div class="overlay"></div>
			<div class="middle">
				<h1>Which St&eacute;rimar <br/><span>is right for me?</span></h1>
			    <p class="link"><a href="#take-the-test" class="quiz">Take The Test Below</a></p>
			</div>
		</div>
	</div>

	<div class="row quiz">
		<a name="take-the-test"></a>
		@include('main.layouts.partials._arrow', ['hrefAnchor' => 'take-the-test'])


		<div class="questions hidden">
			<h1 id="qh1">Who is it for?</h1>
			<h1 id="qh2">How old is your child?</h1>
			<h1 id="qh3">What do you need the treatment for?</h1>
			<h1 id="qh4">What nasal condition does your child suffer from?</h1>
			<h1 id="qh5">What does your child need the treatment for?</h1>
			<h1 id="qh6">What nasal condition do you suffer from?</h1>
			<h1 id="qh-outcome"></h1>

			<div class="numbers">
				<div class="num n1" data-id="1"><span>1</span></div>
				<div class="num n2" data-id="2"><span>2</span></div>
				<div class="num n3" data-id="3"><span>3</span></div>
				<div class="num n4" data-id="4"><span>4</span></div>
				<div class="num n5" data-id="5"><span>5</span></div>
			</div>
		</div>

		<div class="answers hidden">
			<div id="a1" class="answer">
				<div class="columns span-6 md-12 sm-12 o1">
					@include('main.quiz.partials._shim-9x7')
					<a href="#" data-option="1"><span>Myself</span></a>
				</div>
				<div class="columns span-6 md-12 sm-12 o2">
					@include('main.quiz.partials._shim-9x7')
					<a href="#" data-option="2"><span>My Child</span></a>
				</div>
			</div>

			<div id="a2" class="answer">
				<div class="columns span-12 light">
					<a href="#" data-option="1"><span>Birth</span></a>
				</div>
				<div class="columns span-12 mid">
					<a href="#" data-option="2"><span>3 Months - 3 Years</span></a>
				</div>
				<div class="columns span-12 dark">
					<a href="#" data-option="3"><span>3 Years +</span></a>
				</div>
			</div>

			<div id="a3" class="answer">
				<div class="columns span-12 light">
					<a href="#" data-option="1"><span>Aid natural breathing and prevention</span></a>
				</div>
				<div class="columns span-12 mid">
					<a href="#" data-option="2"><span>Ease symptoms</span></a>
				</div>
				<div class="columns span-12 dark">
					<a href="#" data-option="3"><span>Help stop symptoms in their tracks &amp; protect against further attacks</span></a>
				</div>
			</div>

			<div id="a4" class="answer">
				<div class="columns span-12 green">
					<a href="#" data-option="1"><span>Sensitivity to pollen</span></a>
				</div>
				<div class="columns span-12 blue">
					<a href="#" data-option="2"><span>Sensitivity to indoor allergens</span></a>
				</div>
				<div class="columns span-12 orange">
					<a href="#" data-option="3"><span>COLDS &amp; CONGESTION</span></a>
				</div>
			</div>

			<div id="a5" class="answer">
				<div class="columns span-12 light">
					<a href="#" data-option="1"><span>Aid natural breathing and prevention</span></a>
				</div>
				<div class="columns span-12 mid">
					<a href="#" data-option="2"><span>Ease symptoms</span></a>
				</div>
				<div class="columns span-12 dark">
					<a href="#" data-option="3"><span>Help stop symptoms in their tracks &amp; protect against further attacks</span></a>
				</div>
			</div>

			<div id="a6" class="answer">
				<div class="columns span-12 green">
					<a href="#" data-option="1"><span>Sensitivity to pollen</span></a>
				</div>
				<div class="columns span-12 blue">
					<a href="#" data-option="2"><span>Sensitivity to indoor allergens</span></a>
				</div>
				<div class="columns span-12 orange">
					<a href="#" data-option="3"><span>COLDS &amp; CONGESTION</span></a>
				</div>
			</div>


			{{--
				NOTE:
				displayed products defined by variable 'structure' in /resources/assets/js/main/sections/quiz.js -> /public/js/main/sections/quiz.min.js
					structure: {
						...
						'products' : ['cdcs','spcs','crcr'] points to #id of product to display
						...
					}

					<div class="columns span-12 hidden product" id="cdcs">...</div>
					<div class="columns span-12 hidden product" id="spcs">...</div>
					<div class="columns span-12 hidden product" id="crcr">...</div>

					/product images in /public/images/quiz/

				$buyLinks[] defined in App\Http\Controllers\Main\MainController
			--}}

			<div id="a-outcome" class="answer">

				<!-- Stop and Protect Allergy Response -->
				<div class="columns span-12 hidden product" id="spar">
					<div class="row">
						<div class="columns span-4 pack sm-6 sm-before-3">
							<img src="/images/quiz/stop-and-protect-allergy-response.jpg"/>
						</div>

						<div class="columns span-8 sm-12">
							<h2>Stop <span>&amp; Protect</span></h2>
							<h3>Allergy Response</h3>
							<ul class="summary">
								<li>
									<h5>Puts a stop to symptoms fast.</h5>
								</li>
								<li>
									<h5>Reinforced effective protection against further attacks.</h5>
								</li>
								<li>
									<h5>Fast and effective relief when needed most.</h5>
								</li>
							</ul>
						</div>

						<div class="columns span-8 sm-12">
							<div class="buttons">
								<div class="mid">
									<a href="{{{$buyLinks['spar']}}}" target="_blank" class="bn">BUY NOW</a>
									{{-- <button class="cswidget" data-asset-id="91" data-product-sku="DemoSKU-US-1">Buy Now </button> --}}
									<a href="/our-products/allergy#stop-and-protect" class="fom">FIND OUT MORE</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Nasal Hygeine Allergies and Sinusitis -->
				<div class="columns span-12 hidden product" id="nhas">
					<div class="row">
						<div class="columns span-4 pack sm-6 sm-before-3">
							<img src="/images/quiz/nasal-hygeine-allergies-and-sinusitis.jpg"/>
						</div>

						<div class="columns span-8 sm-12">
							<h2>HAYFEVER &amp; ALLERGY RELIEF</h2>
							<h3>RELIEVES ALLERGY SYMPTOMS</h3>
							<ul class="summary">
								<li><h5>Eliminates allergens (pollen, dust, dust mites, pet hair&hellip;) by washing out nasal cavities.</h5></li>
								<li><h5>Prevents allergy symptoms (sneezing, runny noses and itching).</h5></li>
								<li><h5>It has no drugs, preservatives or additives and can be used along side medicated products.</h5></li>
							</ul>
						</div>

						<div class="columns span-8 sm-12">
							<div class="buttons">
								<div class="mid">
									<a href="{{{$buyLinks['nhas']}}}" target="_blank" class="bn">BUY NOW</a>
									{{-- <button class="cswidget" data-asset-id="91" data-product-sku="DemoSKU-US-1">Buy Now </button> --}}
									<a href="/our-products/allergy#nasal-hygeine" class="fom">FIND OUT MORE</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- COLD DEFENCE -->
				<div class="columns span-12 hidden product" id="cdcs">
					<div class="row">
						<div class="columns span-4 pack sm-6 sm-before-3">
							<img src="/images/quiz/cold-defence.jpg"/>
						</div>

						<div class="columns span-8 sm-12">
							<h2>COLD <span>&amp; DEFENCE</span></h2>
							<h3>COLDS &amp; ENT DISORDERS</h3>
							<ul class="summary">
								<li>
									<h5>Helps to limit cold symptoms from developing.</h5>

								</li>
								<li>
									<h5>Washes out impurities in the nose, including airbornes responsible for colds.</h5>

								</li>
								<li>
									<h5>An exclusive formula, inspired by nature.</h5>

								</li>
							</ul>
						</div>

						<div class="columns span-8 sm-12">
							<div class="buttons">
								<div class="mid">
									<a href="{{{$buyLinks['scd']}}}" target="_blank" class="bn">BUY NOW</a>
									{{-- <button class="cswidget" data-asset-id="91" data-product-sku="DemoSKU-US-1">Buy Now </button> --}}
									<a href="/our-products/cold-and-sinus#cold-defence" class="fom">FIND OUT MORE</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Stop and Protect -->
				<div class="columns span-12 hidden product" id="spcs">
					<div class="row">
						<div class="columns span-4 pack sm-6 sm-before-3">
							<img src="/images/quiz/stop-and-protect-cold-and-sinus-relief.jpg"/>
						</div>

						<div class="columns span-8 sm-12">
							<h2>Stop <span>&amp; Protect</span></h2>
							<h3>Cold &amp; Sinusitis Relief</h3>
							<ul class="summary">
								<li>
									<h5>Fast and effective relief from congestion when needed most.</h5>

								</li>
								<li>
									<h5>Protects from further infection.</h5>

								</li>
								<li>
									<h5>FREE FROM Drugs, Steroids and Preservatives.</h5>

								</li>
							</ul>
						</div>

						<div class="columns span-8 sm-12">
							<div class="buttons">
								<div class="mid">
									<a href="{{{$buyLinks['spcs']}}}" target="_blank" class="bn">BUY NOW</a>
									{{-- <button class="cswidget" data-asset-id="91" data-product-sku="DemoSKU-US-1">Buy Now </button> --}}
									<a href="/our-products/cold-and-sinus#stop-and-protect" class="fom">FIND OUT MORE</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Kids Stop and Protect -->
				{{--
					image (on product page): /images/products/baby-stop-and-protect.png
					buy link -> $buyLinks['bsap']
				--}}
				<div class="columns span-12 hidden product" id="kspcs">
					<div class="row">
						<div class="columns span-4 pack sm-6 sm-before-3">
							<img src="/images/quiz/baby-stop-and-protect.jpg"/>
						</div>

						<div class="columns span-8 sm-12">
							<h2>Baby Stop <span>&amp; Protect</span></h2>
							<h3>Cold &amp; Sinusitis Relief</h3>
							<ul class="summary">
								<li><h5>Immediately relieves congestion to help baby breathe easier.</h5></li>
								<li><h5>Helps fight colds by washing out nasal cavities</h5></li>
								<li><h5>100% Natural Sea Water based solution FREE FROM steroids, drugs and preservatives, suitable from 3 months.</h5></li>
							</ul>
						</div>

						<div class="columns span-8 sm-12">
							<div class="buttons">
								<div class="mid">
									<a href="{{{$buyLinks['bsap']}}}" target="_blank" class="bn">BUY NOW</a>
									{{-- <button class="cswidget" data-asset-id="91" data-product-sku="DemoSKU-US-1">Buy Now </button> --}}
									<a href="/our-products/baby-and-kids#baby-stop-and-protect" class="fom">FIND OUT MORE</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Congestion Relief -->
				<div class="columns span-12 hidden product" id="crcr">
					<div class="row">
						<div class="columns span-4 pack sm-6 sm-before-3">
							<img src="/images/quiz/congestion-relief-colds-and-rhinitis.jpg"/>
						</div>

						<div class="columns span-8 sm-12">
							<h2>Congestion <span>Relief</span></h2>
							<h3>Colds &amp; Rhinitis</h3>
							<ul class="summary">
								<li>
									<h5>Powerful yet gentle decongestant, inspired by nature.</h5>

								</li>
								<li>
									<h5>Helps to prevent further infections developing.</h5>

								</li>
								<li>
									<h5>FREE FROM Drugs, Steroids and Preservatives.</h5>

								</li>
							</ul>
						</div>

						<div class="columns span-8 sm-12">
							<div class="buttons">
								<div class="mid">
									<a href="{{{$buyLinks['crcr']}}}" target="_blank" class="bn">BUY NOW</a>
									{{-- <button class="cswidget" data-asset-id="91" data-product-sku="DemoSKU-US-1">Buy Now </button> --}}
									<a href="/our-products/cold-and-sinus#congestion-relief" class="fom">FIND OUT MORE</a>
								</div>
							</div>
						</div>
					</div>
				</div>


				<!-- Toddlers and Kids -->
				<div class="columns span-12 hidden product" id="kcrcr">
					<div class="row">
						<div class="columns span-4 pack sm-6 sm-before-3">
							<img src="/images/quiz/toddlers-and-kids-allergy-response.jpg"/>
						</div>

						<div class="columns span-8 sm-12">
							<h2>Toddlers <span>&amp; Kids</span></h2>
							<h3>Colds &amp; Rhinitis</h3>
							<ul class="summary">
								<li>
									<h5>The effective natural solution that unblocks congested little noses from 3 months plus.</h5>

								</li>
								<li>
									<h5>It helps fight colds and rebalance the nose&rsquo;s natural defences.</h5>

								</li>
								<li>
									<h5>The St&eacute;rimar Kids solution is completely drug and preservative free.</h5>

								</li>
							</ul>
						</div>

						<div class="columns span-8 sm-12">
							<div class="buttons">
								<div class="mid">
									<a href="{{{$buyLinks['kcrcr']}}}" target="_blank" class="bn">BUY NOW</a>
									{{-- <button class="cswidget" data-asset-id="91" data-product-sku="DemoSKU-US-1">Buy Now </button> --}}
									<a href="/our-products/baby-and-kids#toddlers-and-kids" class="fom">FIND OUT MORE</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Congestion Relief -->
				<div class="columns span-12 hidden product" id="bcu">
					<div class="row">
						<div class="columns span-4 pack sm-6 sm-before-3">
							{{-- @include('main.layouts.partials._mumsnet', ['page' => 'quiz']) --}}
							<img src="/images/quiz/baby-clears-and-unblocks.jpg"/>
						</div>

						<div class="columns span-8 sm-12">
							<h2>BREATHE EASY BABY</h2>
							<h3>Clears &amp; Unblocks</h3>
							<ul class="summary">
								<li>
									<h5>Cleans and clears nasal passages from birth.</h5>

								</li>
								<li>
									<h5>Helps prevent colds and aid natural breathing.</h5>

								</li>
								<li>
									<h5>FREE FROM Drugs, Steroids and Preservatives.</h5>

								</li>
							</ul>
						</div>

						<div class="columns span-8 sm-12">
							<div class="buttons">
								<div class="mid">
									<a href="{{{$buyLinks['bcu']}}}" target="_blank" class="bn">BUY NOW</a>
									{{-- <button class="cswidget" data-asset-id="91" data-product-sku="DemoSKU-US-1">Buy Now </button> --}}
									<a href="/our-products/baby-and-kids#clears-and-unblocks" class="fom">FIND OUT MORE</a>
								</div>
							</div>
						</div>
					</div>
				</div>




			</div>
		</div>
	</div>





@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
