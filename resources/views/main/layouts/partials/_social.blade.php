<div class="social {{ $socialclass }}">
	<a href="https://www.facebook.com/SterimarUK" target="_blank"><img src="/images/facebook.svg" class="ico" alt="Facebook"/></a>
	<a href="https://twitter.com/SterimarUK" target="_blank"><img src="/images/twitter.svg" class="ico" alt="Twitter"/></a>
	<a href="https://www.youtube.com/user/SterimarUK" target="_blank"><img src="/images/youtube.svg" class="ico" alt="YouTube"/></a>
</div>