<div class="columns span-12 arrow-block">
	@if (isset($extra))
		{!!$extra!!}
	@endif
	<a href="#<?php echo $hrefAnchor; ?>"><div class="arrow-outer"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" class="arrow">
	<circle cx="257" cy="256" r="180"/>
	<path d="M256,0C114.6,0,0,114.6,0,256s114.6,256,256,256s256-114.6,256-256S397.4,0,256,0z M256,368.1
		L111.9,224.5l23.8-23.9L256,320.5l120.3-119.9l23.8,23.9L256,368.1z"/>
	</svg></div></a>
</div>