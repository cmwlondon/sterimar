<div class="logo">
	<a href="/"><img src="/images/sterimar-logo.png" alt="St&eacute;rimar"/></a>
</div>
<menu class="main">
	<li class="upper two @if ($section == 'about-sterimar') active @endif "><a href="/about-sterimar">About St&eacute;rimar</a><div class="sub"><img src="/images/sub-menu-arrow.svg" class="arr"/><ul>
		<li><a href="/what-is-nasal-hygiene">What&nbsp;is&nbsp;nasal&nbsp;hygiene?</a></li>
		<li><a href="/why-sterimar">Why&nbsp;St&eacute;rimar?</a></li>
		<li><a href="/is-it-for-children">Is&nbsp;it&nbsp;for&nbsp;children?</a></li>
		<li><a href="/how-to-use">How&nbsp;to&nbsp;use&nbsp;it</a></li>
	</ul></div></li>
	<li class="upper @if ($section == 'our-products') active @endif"><a href="/our-products">Our Products</a>
		<div class="sub"><img src="/images/sub-menu-arrow.svg" class="arr"/>
			<ul>
			<li><a href="/our-products/nasal-hygiene-breathe-easy-daily">St&eacute;rimar&nbsp;Breathe&nbsp;Easy&nbsp;Daily</a></li>
			<li><a href="/our-products/allergy#hayfever-relief">St&eacute;rimar&nbsp;Hayfever&nbsp;&amp;&nbsp;Allergy&nbsp;Relief</a></li>
			<li><a href="/our-products/allergy#stop-and-protect">St&eacute;rimar&nbsp;STOP&nbsp;&amp;&nbsp;PROTECT&nbsp;Allergy&nbsp;Response</a></li>
			<li><a href="/our-products/cold-and-sinus#cold-defence">St&eacute;rimar&nbsp;Cold&nbsp;Defence</a></li>
			<li><a href="/our-products/cold-and-sinus#congestion-relief">St&eacute;rimar&nbsp;Congestion&nbsp;Relief</a></li>
			<li><a href="/our-products/cold-and-sinus#stop-and-protect">St&eacute;rimar&nbsp;STOP&nbsp;&amp;&nbsp;PROTECT&nbsp;Cold&nbsp;&amp;&nbsp;Sinusitis&nbsp;Relief</a></li>
			<li><a href="/our-products/baby-and-kids#clears-and-unblocks">St&eacute;rimar&nbsp;Breathe&nbsp;Easy&nbsp;Baby</a></li>
			<li><a href="/our-products/baby-and-kids#toddlers-and-kids">St&eacute;rimar&nbsp;Congestion&nbsp;Relief&nbsp;Kids</a></li>
			<li><a href="/our-products/baby-and-kids#baby-stop-and-protect">St&eacute;rimar&nbsp;Baby&nbsp;STOP&nbsp;&amp;&nbsp;PROTECT&nbsp;Cold</a></li>
			</ul>
		</div>
	</li>
	<li @if  ($section == 'faqs') class="active" @endif><a href="/faqs">FAQs</a></li>
	<li @if  ($section == 'news') class="active" @endif><a href="/news">News</a></li>
	{{--
	<li @if  ($section == 'resources') class="active" @endif><a href="/resources">Resources</a></li>
	<li @if  ($section == 'contact') class="active" @endif><a href="/contact">Contact Us</a></li>
	--}}
	<li @if  ($section == 'where-to-buy') class="active" @endif><a href="/where-to-buy#retailers">Where To Buy</a></li>
	<li @if  ($section == 'hepr') class="active" @endif><a href="/healthcare-professionals">Healthcare Professionals</a></li>
</menu>

<div class="gp-recommended"><img alt="GP RECOMMENDED - No. 1 Brand" src="/images/gp-recommended2.png"></div>

{{-- @include('main.layouts.partials._social', ['socialclass' => 'main']) --}}

<div class="mobile-menu">
	<img src="/images/hamburger.svg" class="burger"/>
	{{-- @include('main.layouts.partials._social', ['socialclass' => 'top']) --}}
</div>

<div class="slide-menu">
	<img src="/images/close.svg" class="close"/>
	<menu>
		<li @if  ($section == 'about-sterimar') class="active" @endif><a href="/about-sterimar">About St&eacute;rimar</a></li>
		<li @if  ($section == 'our-products') class="active" @endif><a href="/our-products">Our Products</a></li>
		<li @if  ($section == 'faqs') class="active" @endif><a href="/faqs">FAQs</a></li>
		<li @if  ($section == 'news') class="active" @endif><a href="/news">News</a></li>
		<li @if  ($section == 'where-to-buy') class="active" @endif><a href="/where-to-buy#retailers">Where To Buy</a></li>
		<li @if  ($section == 'hepr') class="active" @endif><a href="/healthcare-professionals">Healthcare professionals</a></li>
		{{--
		<li @if ($section == 'resources') class="active" @endif><a href="/resources">Resources</a></li>
		<li @if ($section == 'contact') class="active"  @endif><a href="/contact">Contact Us</a></li>
		<li><a href="http://www.sterimaracademy.com/" target="_blank">Healthcare Professionals</a></li>
		--}}
	</menu>
	{{-- @include('main.layouts.partials._social', ['socialclass' => 'mobile']) --}}
</div>
