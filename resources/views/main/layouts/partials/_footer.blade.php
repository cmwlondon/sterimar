<footer>
	<div class="upper">
		<div class="row">
			<div class="columns span-12">
				<h5>Explore St&eacute;rimar</h5>
				{{-- @include('main.layouts.partials._social', ['socialclass' => 'footer']) --}}
			</div>

			<div class="columns col1 span-3 md-6 sm-12">
				<ul>
					<!-- <li><a href="/where-to-buy#retailers" class="wtb_button"><b>Where To Buy</b></a></li> -->
					<li><a href="/about-sterimar">About St&eacute;rimar</a></li>
					<li><a href="/our-products">Our Products</a></li>
					<li><a href="/news">News</a></li>
				</ul>
			</div>
			<div class="columns col2 span-3 md-6 sm-12">
				<ul>
					<li><a href="/terms-and-conditions">Terms &amp; Conditions</a></li>
					<li><a href="/cookie-notice">Cookie Notice</a></li>
					<li><a href="/privacy-policy">Privacy Policy</a></li>
					<li><a href="/contact">Contact Us</a></li>
				</ul>
			</div>
			{{--
			@if (isset($footnote))
			<div class="columns span-12 md-12 sm-12 mb0">
				<div class="footnote">{!! $footnote !!}</div>
			</div>
			@endif
			--}}
			<p class="gprx">*GPrX data: non-medicated nasal sprays, UK, from July 2019 to June 2020 – <a target="_blsnk" href="/pdfs/gprx-data-for-12-months-to-june-2020.pdf" title="download/open PDF: GPrX data for 12 months to June 2020 {opens in new window)">click here for verification</a></p>
<!-- OneTrust Cookies Settings button start -->
<button id="ot-sdk-btn" class="ot-sdk-show-settings">Cookie Settings</button>
<!-- OneTrust Cookies Settings button end -->

		</div>
	</div>
	<div class="lower">
		<p>&copy; Copyright Church &amp; Dwight UK Ltd. 2021. All rights reserved. St&eacute;rimar is a registered trademark of SOFIBEL SAS.</p>
	</div>
</footer>
