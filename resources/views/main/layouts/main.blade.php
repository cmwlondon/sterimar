<!doctype html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1    /DTD/xhtml1-transitional.dtd">
<html class="no-js" lang="en">
	<head>
<!-- OneTrust Cookies Consent Notice start for sterimarnasal.co.uk -->
<script src="https://cdn.cookielaw.org/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="c8f0253e-346d-4999-9161-42ed49a28665" ></script>
<script type="text/javascript">
function OptanonWrapper() { }
</script>
<!-- OneTrust Cookies Consent Notice end for sterimarnasal.co.uk -->

		<meta charset="utf-8" />
		<!-- Chrome Frame -->
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1" />

		<title>{{{$meta['title']}}}</title>
		<meta name="description" content="{{{$meta['desc']}}}" />
		<meta name="robots" content="INDEX, FOLLOW" />

		<link href="https://fonts.googleapis.com/css?family=Handlee|Open+Sans:400,700" rel="stylesheet">
		<link rel="stylesheet" href="/css/main/styles.css?t={{ now()->timestamp }}"/>

		@if (isset($pageViewCSS) && $pageViewCSS != '')
			<link rel="stylesheet" href="/css/{{{$pageViewCSS}}}.css?t={{ now()->timestamp }}"/>
		@endif

		<link rel="canonical" href="{{{$meta['link']}}}" />
		<meta property="og:locale" content="en_GB" />
		<meta property="og:type" content="{{{$meta['pagetype']}}}" />
		<meta property="og:title" content="{{{$meta['title']}}}" />
		<meta property="og:description" content="{{{$meta['desc']}}}" />
		<meta property="og:url" content="{{{$meta['link']}}}" />
		<meta property="og:site_name" content="Sterimar" />
		<meta property="og:image" content="{{{$meta['image']}}}" />

		@include('main.layouts.partials._favicon')

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="/css/animations-ie-fix.css">
			<style type="text/css">
			    #switch-group {display:none;}
			</style>
		<![endif]-->

		<!--[if gte IE 9]>
		  <style type="text/css">
		    .gradient {
		       filter: none!important;
		    }
		  </style>
		<![endif]-->
	</head>

	<body>
		@yield('top')
		<a name="top"></a>

		{{-- cookie banner START --}}
		{{--
		<div class="cookie-disclaimer">To give you the best possible experience this site uses cookies. By continuing to use this website you are giving consent to cookies being used. For more information visit our <a href="/cookie-notice">Cookie&nbsp;Notice</a>.<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 64.6 64.6" enable-background="new 0 0 64.6 64.6" xml:space="preserve" class="btn-close">
			    <line x1="3.5" y1="3.5" x2="61.1" y2="61.1"/>
			    <line x1="61.1" y1="3.5" x2="3.5" y2="61.1"/>
			</svg></div>
		--}}

		{{-- cookie banner END --}}

		<div id="wrapper">
			<div id="mobile-white-out" class="hidden"></div>
			<div id="headerWrapper">
				@include ('main.layouts.partials._main-menu')
			</div>
			<div id="contentWrapper" class="{{{ $class_section }}}">
				<a name="content-section"></a>
				@yield('content')
			</div>
			<div id="footerWrapper">
				@yield('footer')
			</div>
		</div>

		<script>
			var site_url = "{{ $site_url }}";
			@if (isset($jsVars))
				@foreach ($jsVars as $key => $var)
					var {{$key}} = "{{ $var }}";
				@endforeach
			@endif
			@if (isset($pageViewJS))
				var pageViewJS = "{{ $pageViewJS }}";
			@endif
		</script>

		{!! Html::script('js/libs/requirejs/require.js', ['data-main' => '/js/main/main.min.js?1']) !!}

		@if (App::environment() == 'production')

		<!-- Google Analytics -->
		<script type="text/plain" class="optanon-category-2">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-18425779-2']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>

		{{--
	    <!-- Google Tag Manager -->
	    <script>
	        (function (w, d, s, l, i) {
	            w[l] = w[l] || []; w[l].push({
	                'gtm.start':
	                    new Date().getTime(), event: 'gtm.js'
	            }); var f = d.getElementsByTagName(s)[0],
	                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
	                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
	        })(window, document, 'script', 'dataLayer', 'GTM-KRGWWZW');</script>
	    <!-- End Google Tag Manager -->
		--}}
		@endif

		
	</body>
</html>
