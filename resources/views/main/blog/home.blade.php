@extends('main.layouts.main')


@section('top')

@endsection



@section('content')

	<div class="carousel-header intro">
		<div class="bg" style="background-image: url('/uploads/blog/{{{$first['image']}}}');">
			
			<div class="text-heading">
				<h1>{{{$first['title']}}}</h1>
				<h5>{!! \Carbon\Carbon::parse($first['created_at'])->format('jS \\of F') !!}</h5>
			</div>
			<div class="text-extra">
				<p>{{{$first['synopsis']}}}<br/><br/><a href="/news/{{{$first['slug']}}}">read more</a></p>
			</div>

		</div>
	</div>
	
	<div class="row news-list mb5">
		@if (isset($news) && count($news) > 0)
			@foreach ($news as $article)
				<div class="columns span-4 lg-6 md-12 sm-12">
	                <a href="/news/{{{ $article['slug'] }}}">
	                    <div class="row full">
	                    	<div class="columns span-12 thumb">
	                    		<img src="/uploads/blog/thumbs/{{{ $article['thumb'] }}}" alt="{{{ $article['title'] }}}"/>
	                    		<h4>{{{ $article['title'] }}}<br/><span>{!! \Carbon\Carbon::parse($article['created_at'])->format('jS \\of F') !!}</span></h4>
	                        </div>
	                        <div class="columns span-12">
	                        	<p>{{{ $article['synopsis'] }}}<br/><span class="">read more</span></p>
	                        </div>
	                    </div>
	                </a>
	            </div>

			@endforeach

		@else
			
		@endif
	</div>



	@include('main.products.partials._not-sure')







	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
