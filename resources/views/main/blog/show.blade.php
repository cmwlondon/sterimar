@extends('main.layouts.main')


@section('top')
{{--
	<!-- Load Facebook SDK for JavaScript -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7&appId={{{$appID}}}";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
--}}
@endsection


@section('content')

	<div class="row section article">
		<div class="columns span-12 md-12 sm-12">
			<div class="row full">
				<div class="columns span-9 after-3 heading">
					<h1>{{{ $article->title }}}</h1>
					<div class="date">
						{!! $article->created_at->format('F Y'); !!}
					</div>
				</div>
				
				<div class="columns span-12 body">
					{!! $article->body !!}
				</div>

				<div class="columns span-12 nav">
					@if ($prev_link != "")
					<a href="{{$prev_link}}" class="prev">Previous</a>
					@endif
					@if ($next_link != "")
					<a href="{{$next_link}}" class="next">Next</a>
					@endif
				</div>
			</div>
		</div>
		{{--
		<div class="columns span-3 md-12 sm-12">
			<div class="sharer">
				<h5>Share this article</h5>
				<div class="social-links">
					<a href="#" class="bFacebook"><img src="/images/facebook.svg" class="ico" alt="Facebook" title="Facebook"/></a>
					<a href="#" class="bTwitter"><img src="/images/twitter.svg" class="ico" alt="Twitter" title="Twitter"/></a>
					<a href="#" class="bTumblr"><img src="/images/tumblr.svg" class="ico" alt="Tumblr" title="Tumblr"/></a>
				</div>
			</div>
		</div>
		--}}
	</div>

@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
