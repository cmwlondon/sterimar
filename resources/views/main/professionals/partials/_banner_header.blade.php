<div class="carousel hide">
  <div class="slides carousel-inner">
    <div class="slide pane image-4 shift-image-6">
      <img class="image-4-text hidden-small hidden-medium hidden-large hidden-xlarge" src="/images/home/bye-bye-dry.png" alt="" />
      <img class="image-4-text mobile-opcaity  hidden-xxlarge" src="/images/home/bye-bye-dry-alt.png" alt="" />
      <img class="image-4-packshot alt" src="/images/products/pro.png" alt="" />
      <div class="professional-header-btn-wrap">
        <!-- <a href="/professionals/download/leaflet" class="button header-fix">Request Free Samples</a>
        <a href="/professionals/download/leaflet" class="button header-fix">View CPD Video</a> -->
        <!-- <div class="row full sec-0 hidden-medium hidden-small">
  				<div class="columns span-6 md-12 sm-12"> -->
          {{-- <a href="#" class="button header-fix hidden-medium hidden-small video-btn">View CPD video<div class="arrow"></div></a> --}}
  				<!-- </div>
  				<div class="columns span-6 md-12 sm-12"> -->
          {{-- <a href="#" class="button header-fix hidden-medium hidden-small mrg-right form-btn">Request free samples<div class="arrow"></div></a> --}}
  				<!-- </div>
  			</div> -->
      </div>
    </div>
  </div>

  <div class="controls">
    <div class="row">
      <div class="columns span-12 mb0">
        <label class="slide-description"><div class="upper">Replens MD<sup>TM</sup> oestrogen-free, longer lasting vaginal moisturiser</div><br/>Help your patients wave bye-bye to vaginal dryness and hello to happier days to come</label>
        <ul class="carousel-control hidden">
          <li data-id="0" class="active"><a href="#"></a></li>
          <li data-id="1"><a href="#"></a></li>
          <li data-id="2"><a href="#"></a></li>
          <li data-id="3"><a href="#"></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
