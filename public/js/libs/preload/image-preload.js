/**
 * Filename: image-preload.js
 */
define([
		'jquery',
	],
	
	function($) {
		
		$.fn.extend({
			preload: function(imgDir, cb) {	// Image preload
				
				var len = this.length;
				var cur = 0;
				
				this.each(function(index, item) {
					
					var item = item.image;
					
					$('<img/>')
						.load(function() {
							
							if((index+1) === len) {
								if(typeof cb !== 'undefined')
									cb();
							}
							
							cur++;
						})
						.attr('src', ( imgDir === false ? '' : imgDir )+item);
				});
			}
		});
	}
);