<?php
namespace App\Services;

use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AdminService {

    protected $user = null;
    protected $permissions = null;
    protected $auth;

    public function __construct()
    {
        // $this->User = new User;
        $this->user = User::find( Auth::id() );
        $this->permissions = $this->user->role->permissions;
    }

    public function boot() {
    }

    public function register() {
    }

    public function allUsers() {
        return $this->User::all();
    }

    public function user(string $key) {
        return $this->user[$key];
    }

    public function showPermission() {
        return $this->permissions;
    }

    public function checkPermission(string $key) {
        if ($this->permissions != null) {
            foreach ($this->permissions->toArray() as $item)
            {
                if ($item['name'] == $key) {
                    return true;
                }
            }
        }
        return false;
    }

    public function checkPagePermission($key) {
        if ($this->checkPermission($key) == false) {
            view()->share('error','Access Denied');
            App::abort(401);
        }
    }

}