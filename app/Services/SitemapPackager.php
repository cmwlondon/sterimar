<?php namespace App\Services;

use Response;
use Carbon\Carbon;
use Storage;

use App\Models\Blog;

use LSS\Array2XML;


class SitemapPackager {

	public function __construct(Blog $blog)
	{
		Carbon::setLocale('en');

		$this->blog = $blog;

		$this->sitemap = [
			'@attributes' => [
				'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
				'xsi:schemaLocation' => 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd',
				'xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9'
			],

			'url' => []
		];


	}

	public function create() {
		/* */
		$now = new Carbon();
		$now = $now->toW3cString();

		// Home page
		$this->addUrl(url('/'),$now,'monthly','1.0');
		/*
		bug with the original implemantation of the line
		url() -> returns an object instaed of a string, breaking this entire method
		use url('/') instead -> returns the base URL of the site
		*/

		// Landing pages
		$this->addUrl(url('/about-sterimar'),$now,'monthly','0.9');
		$this->addUrl(url('/what-is-nasal-hygiene'),$now,'monthly','0.9');
		$this->addUrl(url('/why-sterimar'),$now,'monthly','0.9');
		$this->addUrl(url('/is-it-for-children'),$now,'monthly','0.9');
		$this->addUrl(url('/how-to-use'),$now,'monthly','0.9');
		$this->addUrl(url('/faqs'),$now,'monthly','0.9');

		$this->addUrl(url('/our-products'),$now,'monthly','0.9');
		$this->addUrl(url('/our-products/allergy'),$now,'monthly','0.9');
		$this->addUrl(url('/our-products/cold-and-sinus'),$now,'monthly','0.9');
		$this->addUrl(url('/our-products/baby-and-kids'),$now,'monthly','0.9');

		// $this->addUrl(url('/quiz'),$now,'monthly','0.9');
		$this->addUrl(url('/contact'),$now,'monthly','0.9');

		$this->addUrl(url('/where-to-buy'),$now,'monthly','0.9');

		$this->addBlog('news');

		$this->addUrl(url('/terms-and-conditions'),$now,'monthly','0.5');
		$this->addUrl(url('/policies'),$now,'monthly','0.5');

		// Convert and save
		// echo "<pre>".print_r($this->sitemap, true)."</pre>";
		// die();

		$xml = Array2XML::createXML('urlset', $this->sitemap);
		// above line generates error:
		// DOMDocument::createTextNode() expects parameter 1 to be string, object given

		$response = $xml->saveXML();
		//dd(Storage::disk('public'));
		Storage::disk('public')->put('sitemap.xml', $response);
		/* */

		$response = Storage::disk('public')->get('sitemap.xml'); // /storage/app/public/sitemap.xml
		
		return Response::make($response, '200')->header('Content-Type', 'text/xml');
	}

	private function addBlog($slug) {
		// $results = $this->blog->published('false')->ordered()->get();
		$results = $this->blog->where('status','=','published')->orderBy('created_at','desc')->get();

		foreach ($results as $blog) {
			$blog_date = Carbon::parse($blog->updated_at);
			$this->addUrl(url($slug.'/'.$blog->slug),$blog_date->toW3cString(),'monthly','0.75');
		}
	}

	private function addUrl($url,$lastmod,$freq,$priority) {
		$this->sitemap['url'][] = ['loc' => $url,
							'lastmod' => $lastmod,
							'changefreq' => $freq,
							'priority' => $priority
							];
	}
}
