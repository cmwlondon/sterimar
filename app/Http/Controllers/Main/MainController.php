<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Session;
use Request;
use Carbon\Carbon;
use App;

class MainController extends Controller {

	public function __construct() {

		// $this->uri = Request::path();

		$this->context = array(
			'pageViewCSS' 		=> "",
			'pageViewJS'			=> "",
			'site_url'				=> url('',[]),
			'section'					=> Request::segment(1),
			'class_section'		=> "",
			'meta' => [
				'title'	=> 'St&eacute;rimar - Natural Sea Water Nasal Spray',
				'link'	=> url(Request::path()),
				'pagetype' => 'article',
				'desc'	=> '',
				'image' => url('/images/sterimar-logo.png')
			],
			'buyLinks' => [
				// 'spar' 	=> 'http://www.boots.com/en/Sterimar-Stop-and-Protect-Allergy-Response-Nasal-Spray_1658644/',
				// 'nhas' 	=> 'http://www.boots.com/en/Sterimar-Nasal-Hygiene-Spray-50ml_1802413/',
				// 'spcs' 	=> 'http://www.boots.com/en/Sterimar-Stop-and-Protect-Cold-Sinus-Relief-20ml_1658638/',
				// 'crcr' 	=> 'http://www.boots.com/en/Sterimar-Hypertonic-Congestion-Relief-100ml_851836/',
				// 'kcrcr' => 'http://www.boots.com/en/Sterimar-Kids-Congestion-Relief-50ml_1546570/',
				// 'bcu' 	=> 'http://www.boots.com/en/Sterimar-Baby-Nasal-Hygiene-0-3-years-50ml-_1116574/'
								'nasalhygiene' => 'https://www.boots.com/sterimar-isotonic-nasal-hygiene-spray-100ml-10028386',
								'spar' 	=> 'http://www.boots.com/sterimar-stop-and-protect-allergy-response-nasal-spray-10191794',
								'nhas' 	=> 'https://www.boots.com/sterimar-hayfever-allergy-relief-50ml-10276493',
								'spcs' 	=> 'http://www.boots.com/sterimar-stop-and-protect-cold-and-sinus-relief-20ml-10191795',
								'crcr' 	=> 'http://www.boots.com/sterimar-hypertonic-congestion-relief-100ml-10081013',
								'kcrcr' => 'http://www.boots.com/sterimar-kids-congestion-relief-50ml-10185518',
								'bcu' 	=> 'http://www.boots.com/sterimar-baby-nasal-hygiene-0-3-years-50ml-10112027',
								'scd' 	=> 'http://www.boots.com/sterimar-cold-defence-nasal-spray-50ml-10235901',
								'bsap' 	=> 'https://www.boots.com/sterimar-stop-and-protect-baby-nasal-spray-10252822',
			]
		);

		// $meta = config('meta');
		// if (array_key_exists($this->uri,$meta)) {
		// 	foreach($this->context['meta'] as $key => $value) {
		// 		if (array_key_exists($key,$meta[$this->uri])) {
		// 			if ($key == "title" && $meta[$this->uri][$key] == "") {
		// 				// Ignore blank titles;
		// 			} else {
		// 				$this->context['meta'][$key] = $meta[$this->uri][$key];
		// 			}
		// 		}
		// 	}
		// }

		// Work out if we're in spring/summer or autumn/winter
		$now = Carbon::now();
		$first = Carbon::createFromDate($now->year, 3, 1, 'Europe/London');
		$second = Carbon::createFromDate($now->year, 9, 1, 'Europe/London');
		$isSummer = $now->between($first, $second);
		$this->context['isWinter'] = !$isSummer;

		switch (App::environment()) {
		    case 'local':
		    case 'development':
		        $appID = '540752406118442';
		        break;
		    case 'testing':
		    case 'staging':
		        $appID = '540752576118425';
		        break;
		    case 'live':
		    case 'production':
		        $appID = '540751982785151';
		        break;
		}

		$this->context['appID'] = $appID;
	}
}
