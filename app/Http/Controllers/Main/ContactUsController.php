<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

use App\Models\Contact;

use App\Http\Requests\ContactRequest;

use Mail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ContactUsController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		$this->context['pageViewJS']	= ''; //'main/sections/contact.min';
		$this->context['pageViewCSS']	= 'main/sections/contact';

		$this->context['titlesArr'] = ['' => 'Select', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Ms' => 'Ms'];

		$this->context['meta']['title']	= 'Sterimar Allergy Relief Microdiffusion Spray - Contact Us';
		$this->context['meta']['desc']	= 'Looking for more info on Stérimar products? Contact our customer careline here. Whatever you need to know, our trained advisors are here to help.';

		return view('main.contact.home', $this->context);
	}

	public function store(ContactRequest $request, Contact $contact)
	{

		$details = array(
			'title'				=> $request->title,
			'firstname'		=> $request->firstname,
			'lastname'		=> $request->lastname,
			'company'			=> '',//$request->company,
			'address'			=> '',//$request->address,
			'city'				=> '',//$request->city,
			'county'				=> '',//$request->county,
			'postcode'				=> '',//$request->postcode,
			'country'			=> '',//$request->country,
			'phone'				=> $request->phone,
			'email'				=> $request->email,
			'product'			=> ($request->product !== '' && !is_null($request->product)) ? $request->product : '-',
			'comments'		=> $request->comments
		);

		$contact->fill($details);
		$contact->save();

		if(getenv('APP_ENV') === 'production'){
			$this->sendSYsMail($details);
			// $this->sendMail($details);
		}

		$this->response = array('response_status' => 'success', 'message' => 'Thank you for contacting us.');

		return redirect('contact')->with('response', $this->response);
	}


	// private function sendMail($details)
	// {
	// 	Mail::send('emails.contact', $details, function($message) use($details)
	// 	{
	// 	    $message->from(env('MAIL_NOREPLY'), env('MAIL_NAME'));
	// 	    $message->to(env('MAIL_ADMIN_TO'));
	//
	// 	    if (!is_null(env('MAIL_ADMIN_CC')) && env('MAIL_ADMIN_CC') != "") {
	// 	    	$message->cc(env('MAIL_ADMIN_CC'));
	// 	    }
	// 	    if (!is_null(env('MAIL_ADMIN_BCC')) && env('MAIL_ADMIN_BCC') != "") {
	// 	    	$message->bcc(env('MAIL_ADMIN_BCC'));
	// 	    }
	// 	    $message->subject('Pearl Drops Enquiry');
	// 	});
	// }

	private function sendSYsMail($details)
	{
		Mail::send(['text' => 'emails.enquiry'], $details, function($message) use($details)
		{
		    $message->from(env('MAIL_NOREPLY'), env('MAIL_NAME'));
		    $message->to(env('MAIL_SYS_TO'));
		    if (env('MAIL_ADMIN_CC',"") != "") {
				$message->bcc(env('MAIL_ADMIN_CC'));
			}
		    if (env('MAIL_ADMIN_BCC',"") != "") {
				$message->bcc(env('MAIL_ADMIN_BCC'));
			}
		    $message->subject('UK ChurchDwight Enquiry');
		});
	}

	public function mailtest()
	{

		// Prepare the email to send.
		$details = array(
			'title'				=> 'TITLE',
			'firstname'		=> 'firstname',
			'lastname'		=> 'lastname',
			'company'			=> '',//$request->company,
			'address'			=> '',//$request->address,
			'address_2'			=> '',//$request->address_2,
			'city'				=> '',//$request->city,
			'state'				=> '',//$request->county,
			'zip'				=> '',//$request->postcode,
			'country'			=> '',//$request->country,
			'phone'				=> 'phone',
			'email'				=> 'email',
			'jobTitle'			=> '',
			'product'			=> 'product',
			'comments'		    => 'comments',
			'mfgcode'			=> 'sterimar'
		);

		$this->sendSYsMail($details);
	}

}
