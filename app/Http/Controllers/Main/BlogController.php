<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

use App\Models\Blog;

use Request;

class BlogController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Blog $blog)
	{
		parent::__construct();
		$this->blog = $blog;
	}

	/**
	 * Show the application blog screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']	= 'main/sections/blog.min';
		$this->context['pageViewCSS']	= 'main/sections/blog';

		$this->context['meta']['title']	= 'Sterimar News - Sea Water Microdiffusion Spray For Allergies';
		$this->context['meta']['desc']	= 'Read the latest news from Stérimar the natural sea water nasal spray for treatment against allergies and congestion. Read more about nasal hygiene.';

		$this->context['news'] = $this->blog->published(false)->orderedDate()->get()->toArray();

		$this->context['first'] = array_shift($this->context['news']);
		return view('main.blog.home', $this->context);
	}

	/**
	 * Show the application blog view screen to the user.
	 *
	 * @return Response
	 */
	public function show($slug)
	{
		$this->context['pageViewJS']	= 'main/sections/blog.min';
		$this->context['pageViewCSS']	= 'main/sections/blog';

		$preview = Request::input('preview');

		$article = $this->blog->slug($slug)->published($preview)->get()->first();
		if (is_null($article)) {
			abort(404);
		}

		$prev = $this->blog->published($preview)->older($article->created_at)->get()->first();
		$next = $this->blog->published($preview)->newer($article->created_at)->get()->first();


		$this->context['prev_link'] = ($prev == null) ? "" :"/news/" . $prev->slug;
		$this->context['next_link'] = ($next == null) ? "" :"/news/" . $next->slug;

		$this->context['article'] = $article;

		$this->context['meta']['title']	= ($article->meta_title == '') ? $article->title : $article->meta_title;
		$this->context['meta']['desc'] = ($article->meta_description == '') ? $article->synopsis : $article->meta_description;
		$this->context['meta']['image'] = ($article->image == '') ? (($article->thumb == '') ? $this->context['meta']['image'] : url('/uploads/blog/thumbs/'.$article->thumb)) : url('/uploads/blog/'.$article->image);

		return view('main.blog.show', $this->context);
	}
}
