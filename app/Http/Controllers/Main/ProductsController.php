<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class ProductsController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']	= 'main/sections/products.min';
		$this->context['pageViewCSS']	= 'main/sections/products';
		$this->context['meta']['title']	= 'Sterimar Products - Sea Water Microdiffusion Spray';
		$this->context['meta']['desc']	= 'Learn more about Stérimar\'s range of effective and drug-free nasal spray solutions inspired by nature. View our allergy and hayfever solutions.';

		return view('main.products.home', $this->context);
	}

	public function view($product)
	{
		$ranges = ["allergy","cold-and-sinus","baby-and-kids","nasal-hygiene-breathe-easy-daily"];
		if (!in_array($product,$ranges)) {
			abort(404, 'Product range not found.');
		}

		$metaArray = array(
			"allergy" => [
				 'title' => 'Allergy Relief - Sterimar Sea Water Microdiffusion Spray'
				,'desc'	 => 'Find out more about the range of allergy and hayfever relief solutions from Stérimar - from prevention to fast and effective relief from symptoms.'
			],
			"baby-and-kids" => [
				 'title' => 'Clear & Unblock Baby & Kids Noses - Sterimar Allergy Relief'
				,'desc'	 => 'Browse the Stérimar product range for babies, toddlers and children - our range of specially designed solutions to clear and unblock little noses.'
			],
			"cold-and-sinus" => [
				 'title' => 'Cold & Sinus Relief - Relief From Decongestion By Sterimar'
				,'desc'	 => 'Stérimar offer a range of cold & congestion solutions - from the first sign of symptoms to more powerful rapid relief. Find out more.'
			],
			"nasal-hygiene-breathe-easy-daily" => [
				 'title' => 'Nasal Hygiene By Sterimar'
				,'desc'	 => 'Nasal Hygiene By Sterimar'
			]
		);

		$this->context['meta']['title']				= $metaArray[$product]['title'];
		$this->context['meta']['desc']				= $metaArray[$product]['desc'];
		$this->context['pageViewJS']	= 'main/sections/products.min';
		$this->context['pageViewCSS']	= 'main/sections/products';

		return view('main.products.'.$product, $this->context);
	}

	public function wtb()
	{
		$this->context['pageViewJS']		= 'main/sections/wtb.min';
		$this->context['pageViewCSS']		= 'main/sections/products';
		$this->context['meta']['title']	= 'Where To Buy Sterimar - Sea Water Microdiffusion Spray';
		$this->context['meta']['desc']	= 'You can buy Stérimar in Boots, Tesco, Morrisons, Lloyds Pharmacy, independent pharmacies & most supermarket pharmacies. Find out more on our website.';

		return view('main.products.wtb', $this->context);
	}
}
