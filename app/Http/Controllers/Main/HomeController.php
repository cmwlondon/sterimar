<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

use App\Models\Blog;

class HomeController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Blog $blog)
	{
		parent::__construct();
		$this->blog = $blog;
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']	= 'main/sections/home.min';
		$this->context['pageViewCSS']	= 'main/sections/home';

		$this->context['meta']['title'] = 'Sterimar Allergy Relief - Sea Water Microdiffusion Spray';
		$this->context['meta']['desc']  = 'Stérimar natural sea water nasal spray helps to restore the nose&rsquo;s natural functions, treating allergies and blocked noses. Find out more at Sterimar.';

		$this->context['news'] = $this->blog->published(false)->orderedDate()->limit(3)->get();

		return view('main.home.home', $this->context);
	}
}
