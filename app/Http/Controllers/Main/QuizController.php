<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class QuizController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']	= 'main/sections/quiz.min';
		$this->context['pageViewCSS']	= 'main/sections/quiz';

		$this->context['meta']['title']	= 'Which Sterimar Allergy Relief Spray Is Right For You - Quiz';
		$this->context['meta']['desc']	= 'Which Stérimar Nasal Spray is right for you? Take our online interactive quiz to help you choose the right product for you or for your child.';

		return view('main.quiz.home', $this->context);
	}
}
