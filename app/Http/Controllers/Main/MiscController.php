<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use Mail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Services\SitemapPackager;

class MiscController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->context['section'] = '';
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */

	
	public function hepr()
	{
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= 'main/sections/healthcare-professionals';
		$this->context['section'] = 'hepr';

		$this->context['meta']['title']	= 'Sterimar Allergy Relief Spray - Healthcare Professionals';
		$this->context['meta']['desc']	= 'healthcare professional academy for GPs, mdiwives and pharmacists';

		return view('main.misc.hepr', $this->context);
	}

	public function terms()
	{
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= '';

		$this->context['meta']['title']	= 'Sterimar Allergy Relief Spray - Terms & Conditions';
		$this->context['meta']['desc']	= 'Read our terms and conditions concerning your use of our website, intellectual property, liability, site links, and other important terms.';

		return view('main.misc.terms', $this->context);
	}

	public function cookies()
	{
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= '';

		$this->context['meta']['title']	= 'Sterimar Allergy Relief - Cookies Policy';
		$this->context['meta']['desc']	= 'Church & Dwight UK have established an Internet Privacy Policy regarding the privacy of your info throughout your use of this website. Learn more.';

		return view('main.misc.cookies', $this->context);
	}

	public function privacy()
	{
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= '';

		$this->context['meta']['title']	= 'Sterimar Allergy Relief - Privacy Policy';
		$this->context['meta']['desc']	= 'Church & Dwight UK have established an Internet Privacy Policy regarding the privacy of your info throughout your use of this website. Learn more.';

		return view('main.misc.privacy', $this->context);
	}

	public function thirdparty()
	{
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= '';

		$this->context['meta']['title']	= 'Sterimar Allergy Relief - Third Party Information Collection';
		$this->context['meta']['desc']	= 'Church & Dwight UK Third Party Information Collection';

		return view('main.misc.third-party', $this->context);
	}

	public function mailtest()
	{
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= '';

		$this->context['meta']['title']	= 'Sterimar Allergy Relief - mailtest';
		$this->context['meta']['desc']	= 'mailtest';

		$details = array(
			'title'			=> 'Mr',
			'firstname'		=> 'Albert',
			'lastname'		=> 'Einstein',
			'phone'			=> '7542087301',
			'email'			=> 'albert.einstain@gmail.com',
			'product'		=> 'orajel mailgun test product',
			'comments'		=> 'orajel meilgun test comment',
			'mfgcode'		=> 'sentinent.orajel.088',
			'timestamp' 	=> date("Y-m-d H:i:s"),
			'company'		=> '',
			'address'		=> '',
			'city'			=> '',
			'county'		=> '',
			'postcode'		=> '',
			'country'		=> ''
		);

		Mail::send(['text' => 'emails.enquiry'], $details, function($message) use($details)
		{
			$message->from('sterimar@sandbox677aa5c46b624036a28a2e15eb445c02.mailgun.org', 'sterimar');
			$message->to('hockey.richard@gmail.com'); // address in list of authorised test addresses for sandbox
		    $message->subject('UK ChurchDwight Enquiry');
		});

		return view('main.misc.mailtest', $this->context);
	}

	public function sitemap(SitemapPackager $packager)
	{

		return $packager->create();
	}
}
