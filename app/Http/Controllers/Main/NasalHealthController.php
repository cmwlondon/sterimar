<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

use Response;

class NasalHealthController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->context['section'] = 'about-sterimar';
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= 'main/sections/nasalhealth';
		
		$this->context['meta']['title']	= 'Nasal Hygiene - Sterimar Sea Water Microdiffusion Spray';
		$this->context['meta']['desc']	= 'What is nasal hygiene? Keeping the nose clean & moisturised ensures your nose is in top working condition & defends against airborne contaminants.';

		return view('main.nasalhealth.home', $this->context);
	}

	public function whatis()
	{
		$this->context['pageViewJS']	= 'main/sections/what-is-sterimar-nasal.min';
		$this->context['pageViewCSS']	= 'main/sections/nasalhealth';

		$this->context['meta']['title']	= 'What Is Nasal Hygiene - Sterimar Stop & Protect Allergies';
		$this->context['meta']['desc']	= 'Your nose is the first line of defence against dust, pollution & allergens that may harm your body. Use Stérimar to support nasal hygeine.';

		return view('main.nasalhealth.what-is-it', $this->context);
	}

	public function why()
	{
		$this->context['pageViewJS']	= 'main/sections/why-sterimar.min';
		$this->context['pageViewCSS']	= 'main/sections/nasalhealth';

		$this->context['meta']['title']	= 'Why Sterimar Stop & Protect - Allergy Relief Spray';
		$this->context['meta']['desc']	= 'Our Stérimar Sea Water Nasal Spray is 100% natural, meaning no use of steroids and corticoids that can be found in other products. Find out more.';

		return view('main.nasalhealth.why-sterimar', $this->context);
	}

	public function children()
	{
		$this->context['pageViewJS']	= 'main/sections/for-children.min';
		$this->context['pageViewCSS']	= 'main/sections/nasalhealth';

		$this->context['meta']['title']	= 'Is Sterimar For Children - Sea Water Microdiffusion Spray';
		$this->context['meta']['desc']	= 'Is Stérimar suitable for kids & babies? Find out why it\'s important to keep their noses clear & read about effective nasal relief for the family.';

		return view('main.nasalhealth.is-it-for-children', $this->context);
	}

	public function useage()
	{
		$this->context['pageViewJS']	= 'main/sections/useage.min';
		$this->context['pageViewCSS']	= 'main/sections/nasalhealth';

		$this->context['meta']['title']	= 'How To Use Sterimar Microdiffusion Spray For Allergies';
		$this->context['meta']['desc']	= 'See our instructions for use of Stérimar products with adults, babies, and children. Download instructions in a PDF or watch a short video.';

		return view('main.nasalhealth.how-to-use', $this->context);
	}

	public function faqs()
	{
		$this->context['section'] = 'faqs';
		$this->context['pageViewJS']	= 'main/sections/faqs.min';
		$this->context['pageViewCSS']	= 'main/sections/nasalhealth';

		$this->context['meta']['title']	= 'Sterimar Allergy Relief FAQ - Sea Water Microdiffusion Spray';
		$this->context['meta']['desc']	= 'Read our frequently asked questions to find out about Stérimar solutions. Learn more about the products we stock & our recommendations for them here.';

		return view('main.nasalhealth.faqs', $this->context);
	}

	public function download($slug)
	{
		$files = [	"adults-and-children" => "Instructions for use with adults and children.pdf",
					"babies" => "Instructions for use with babies.pdf"
				];
		if (!array_key_exists($slug,$files)) {
			abort(404);
		}

		$tmp_file = $files[$slug];

		return Response::download(public_path()."/pdfs/".$tmp_file, $tmp_file, ['Content-Type: application/pdf']);
	}
}
