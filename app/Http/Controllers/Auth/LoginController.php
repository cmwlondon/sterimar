<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->context = array();
        $this->context['pageViewCSS'] = '';
        $this->context['pageViewJS'] = '';
    }

    // override method 'showLoginForm' in /vendors/laravel/framework/src/Illuminate/Foundation/Auth/AuthenticateUsers.php
    // use custom login form
    public function showLoginForm()
    {
        return view('admin.auth.login',$this->context);
    }    
}
