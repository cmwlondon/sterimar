<?php namespace App\Http\Controllers\Admin\Blog;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests;

use Request;

//use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;

use Config;
use Admin;

use App\Models\Blog;
use App\Models\Categories;

use App\Http\Requests\Admin\BlogRequest as BlogRequest;

use Intervention\Image\ImageManagerStatic as Image;

use App\Services\SitemapPackager;

class BlogController extends AdminController {

	/**
	 * Create a new controller instance with dependency injection
	 *
	 * @return void
	 */
	public function __construct(Guard $guard, Blog $blog, Categories $categories, SitemapPackager $packager) {

		parent::__construct($guard);

		$this->blog = $blog;
		$this->categories = $categories;
		$this->packager = $packager;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['title'] = 'Blog Management';

		$blog = $this->blog->ordered()->get();

		$blog->each(function($post)
		{
		    $post->category;
		});
		//dd($blog);
		$this->context['blog'] = $blog;

		return view('admin.blog.main', $this->context);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->context['title']				= 'Create Blog';
		$this->context['categories']		= array_pluck($this->categories->all(),'title','id');

		$this->context['pageViewCSS']		= '';
		$this->context['pageViewJS']		= 'admin/blog-create-edit.min';

		return view('admin.blog.create', $this->context);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = $this->blog->findOrFail($id);
		//dd($post->categories->all()->to);

		$post->current_categories = array_pluck($post->categories->all(),'title','id');
		//dd($post->current_categories);

		$this->context['title'] = 'Blog Management : Editing ' . $post->title;

		$this->context['post'] 			= $post;

		$this->context['categories']		= array_pluck($this->categories->all(),'title','id');

		$this->context['pageViewCSS']		= '';
		$this->context['pageViewJS']		= 'admin/blog-create-edit.min';

		return view('admin.blog.edit', $this->context);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(BlogRequest $request)
	{

		$thumb = $this->processThumbImage($request);
		$image = $this->processMainImage($request);

		$order = $this->blog->get()->count() + 1;

		$data = [
					'revision'			=> time(),
					'status'			=> $request->status,
					'title'				=> $request->title,
					'slug'				=> $request->slug,
					'thumb'      		=> $thumb,
					'image'      		=> $image,
					'synopsis'			=> $request->synopsis,
					'body'				=> $request->body,
					'meta_title'		=> ($request->meta_title !== NULL) ? $request->meta_title : '-',
					'meta_description'	=> ($request->meta_description !== NULL) ? $request->meta_description : '-',
					'order'				=> $order
				];

	  	$this->blog->fill($data);


		$attach_ids = [];
		$categories = array_pluck($this->categories->all(),'title','id');
		foreach($categories as $id => $cat) {
			if ($request['cat_'.$id] == 1) {
				$attach_ids[] = $id;
			}
		}

		$this->blog->save();

		$this->blog->categories()->detach();
		$this->blog->categories()->attach($attach_ids);

		$pkg_response = $this->packager->create(); // Update the Sitemap

		$this->response = array('response_status' => 'success', 'message' => 'This post has been added successfully.');

		return redirect('admin/blog')->with('response', $this->response);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, BlogRequest $request)
	{
		$post = $this->blog->findOrFail($id);

		$data = [
					'status'			=> $request->status,
					'title'				=> $request->title,
					'slug'				=> $request->slug,
					'synopsis'		=> $request->synopsis,
					'body'				=> $request->body,
					'meta_title'		=> ($request->meta_title !== NULL) ? $request->meta_title : '-',
					'meta_description'	=> ($request->meta_description !== NULL) ? $request->meta_description : '-',
					'created_at'				=> $request->created_at
				];


		if ($request->file('image') != null) {
			$image = $this->processMainImage($request);
			$data['image'] = $image;
		} else {
			if ($request->clear_image == 1) {
				$data['image'] = "";
			}
 		}

		if ($request->file('thumb') != null) {
			$thumb = $this->processThumbImage($request);
			$data['thumb'] = $thumb;
		} else {
			if ($request->clear_thumb == 1) {
				$data['thumb'] = "";
			}
 		}

		$post->update($data);

		$attach_ids = [];
		$categories = array_pluck($this->categories->all(),'title','id');
		foreach($categories as $id => $cat) {
			if ($request['cat_'.$id] == 1) {
				$attach_ids[] = $id;
			}
		}

		$post->categories()->sync($attach_ids);

		//$post->save();

		$pkg_response = $this->packager->create(); // Update the Sitemap

		$this->response = array('response_status' => 'success', 'message' => 'This post has been updated successfully.');

		return redirect('admin/blog')->with('response', $this->response);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$post = $this->blog->findOrFail($id);

		if ($post->thumb != "") {
			$thumb = public_path().'/uploads/blog/thumbs/'.$post->thumb;
			if (file_exists($thumb)) {
				unlink ($thumb);
			}
		}

		if ($post->image != "") {
			$image = public_path().'/uploads/blog/'.$post->image;
			if (file_exists($image)) {
				unlink ($image);
			}
		}


		$post->delete();

		$pkg_response = $this->packager->create(); // Update the Sitemap

		$this->response = array('response_status' => 'success', 'message' => 'This post has been deleted successfully.');

		return redirect('admin/blog')->with('response', $this->response);
	}



	private function processThumbImage($request) {
		$filename = $request->slug . '-' . time() . '.' . $request->file('thumb')->getClientOriginalExtension();

		// /storage/app/public === Storage::disk('public')
		// Storage::disk('public')->put('sitemap.xml', $response);

		// /public/uploads/
		$request->file('thumb')->move(
	        public_path().'/uploads/blog/thumbs/', $filename
	    );
		// open file a image resource
		$img = Image::make('uploads/blog/thumbs/'.$filename);

		$img->fit(500)->save();
		return $filename;
	}

	private function processMainImage($request) {
		$filename = $request->slug . '-' . time() . '.' . $request->file('image')->getClientOriginalExtension();

		// /public/uploads/
		$request->file('image')->move(
	        public_path().'/uploads/blog/', $filename
	    );

		// open file a image resource
		$img = Image::make('uploads/blog/'.$filename);

		$img->fit(1920,775, function ($constraint) {
				//$constraint->aspectRatio();
			    $constraint->upsize();
			})->save();
		return $filename;
	}
}
