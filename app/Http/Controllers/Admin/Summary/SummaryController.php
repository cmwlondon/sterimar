<?php

namespace App\Http\Controllers\Admin\Summary;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Http\Request;

class SummaryController extends AdminController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->context['title'] = 'System Summary';
        $this->context['pageViewJS'] = '';
        return view('admin.summary.home', $this->context);
    }
}
