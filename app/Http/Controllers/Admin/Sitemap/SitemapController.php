<?php namespace App\Http\Controllers\Admin\Sitemap;

use App\Http\Controllers\Admin\AdminController;

use Illuminate\Contracts\Auth\Guard;

use Admin;

use App\Services\SitemapPackager;

class SitemapController extends AdminController {

	/**
	 * Create a new controller instance with dependency injection
	 *
	 * @return void
	 */
	public function __construct(Guard $guard) {
		
		parent::__construct($guard);
	}


	/**
	 * 
	 *
	 * @return Response
	 */
	public function refresh(SitemapPackager $packager)
	{
		$pkg_response = $packager->create();
		return $pkg_response;
	}
}
