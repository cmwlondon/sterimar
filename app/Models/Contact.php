<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

		/**
		 * The database table used by the model.
		 *
		 * @var string
		 */
		protected $table = 'contacts';

		public $timestamps = true;

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
		 'title'
		,'firstname'
		,'lastname'
		,'address'
		,'city'
		,'postcode'
		,'country'
		,'phone'
		,'email'
		,'product'
		,'comments'
		];

		/**
		* The attributes excluded from the model's JSON form.
		*
		* @var array
		*/
		protected $hidden = [];
}
