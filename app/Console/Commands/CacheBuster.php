<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CacheBuster extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cachebuster:update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generates a new random string to be used against file pointers througout the site. Forces browsers to pull fresh content from the server.';

	/**
	* Standard random string length.
	*
	* @var string
	*/
	protected $len = 30;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

	    $env_update = $this->updatedEnvRandomString();

	    if($env_update){
				$this->info('CacheBuster created random string '.$this->len.' characters long.');
	    } else {
				$this->error('There was an error writing CacheBuster string!! Check .env file exist.');
	    }
	}

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $env_update = $this->updatedEnvRandomString();

	    if($env_update){
				$this->info('CacheBuster created random string '.$this->len.' characters long.');
	    } else {
				$this->error('There was an error writing CacheBuster string!! Check .env file exist.');
	    }
    }

	public function updatedEnvRandomString()
	{

		if(file_exists(base_path() . '/.env')) {

			// Read .env-file
			$env = file_get_contents(base_path() . '/.env');
			// set flag to determine cachebuster exists in .env
			$foundCachebuster = false;
			// cast int on strlen argument to filter numbers only
			$strlen = (int)$this->argument('strlen');

			// check to see if number valid and not 0
			// or else use default
			if(isset($strlen) && is_int($strlen) && $strlen > 0)
			$this->len = $strlen;

			// Split string on every " " and write into array
			$arrayEnv = preg_split('/\s+/', $env);
			// loop through array and explode on "=" to find "CacheBuster"
			// if so, update random value
			foreach($arrayEnv as $env_key => $env_value) {
				$entry = explode("=", $env_value);
				if($entry[0] == "CACHE_BUSTER") {
					$foundCachebuster = true;
					$arrayEnv[$env_key] = "CACHE_BUSTER=".str_random($this->len)."\n";
				}
			}

			// if "CacheBuster" not found then append line with random value
			// collapse array with new value otherwise
			if(!$foundCachebuster)
				$env .= "CACHE_BUSTER=".str_random($this->len)."\n";
			else
				$env = implode("\n", $arrayEnv);

			return file_put_contents(base_path() . '/.env', $env);
		}
		else {
			return false;
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['strlen', InputArgument::OPTIONAL, 'The length of random string generated.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			// ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
