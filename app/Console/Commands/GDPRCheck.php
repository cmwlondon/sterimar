<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Contact;
use Carbon\Carbon;

class GDPRCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gdpr:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks supports table for entries over 3 months old and obscures the data.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Contact $support)
    {
        $data = $support->where('firstname','<>','GDPR')->whereDate('created_at', '<=', Carbon::today()->subMonths(3)->toDateString() )->get();
        foreach($data as $key => $entry) {
            $entry->title = '-';
            $entry->firstname = 'GDPR';
            $entry->lastname = '-';
            $entry->address = '-';
            $entry->city = '-';
            $entry->postcode = '-';
            $entry->country = '-';
            $entry->phone = '-';
            $entry->email = $entry->created_at->timestamp . '@gdpr.com';
            $entry->product = '-';
            $entry->comments = '-';
            $entry->save();
            $this->line($key);
        }
    }
}
